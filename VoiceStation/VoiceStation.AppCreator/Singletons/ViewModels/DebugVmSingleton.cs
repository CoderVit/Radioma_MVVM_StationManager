﻿using VoiceStation.AppCreator.Builders;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;
using VoiceStation.MVVM.Models.Windows;

namespace VoiceStation.AppCreator.Singletons.ViewModels
{
    public class DebugVmSingleton
    {
        private IMediator m_mediator;
        private SinHelperBuilder m_sinHelperBuilder;

        private DebugViewModel m_debugViewModel;

        #region Builder Methods

        public DebugVmSingleton WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public DebugVmSingleton WithSinHelperBuilder(SinHelperBuilder sinHelperBuilder)
        {
            m_sinHelperBuilder = sinHelperBuilder;
            return this;
        }

        #endregion Builder Methods

        public DebugViewModel Instance
        {
            get
            {
                if (m_debugViewModel != null) return m_debugViewModel;
                m_debugViewModel = new DebugViewModel(m_sinHelperBuilder.Build());

                m_mediator.Register(StationMessage.STATION_MESSAGE_FIRST_LOST_PACKAGE, () => ++m_debugViewModel.LostPackages[0]);
                m_mediator.Register(StationMessage.STATION_MESSAGE_SECOND_LOST_PACKAGE, () => ++m_debugViewModel.LostPackages[1]);
                m_mediator.Register(StationMessage.STATION_MESSAGE_THIRD_LOST_PACKAGE, () => ++m_debugViewModel.LostPackages[2]);
                m_mediator.Register(StationMessage.STATION_MESSAGE_FOURTH_LOST_PACKAGE, () => ++m_debugViewModel.LostPackages[3]);

                return m_debugViewModel;
            }
        }
    }
}