﻿namespace VoiceStation.AppHelper.Abstract
{
    public interface IProcessInvoker
    {
        void Invoke();
    }
}