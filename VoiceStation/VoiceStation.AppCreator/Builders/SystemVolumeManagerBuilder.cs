﻿using NAudio.CoreAudioApi;
using VoiceStation.AudioHelper.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class SystemVolumeManagerBuilder
    {
        public SystemVolumeManager Build()
        {
            return new SystemVolumeManager(new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia));
        }
    }
}