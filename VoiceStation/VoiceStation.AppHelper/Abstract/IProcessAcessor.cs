﻿using System.Diagnostics;

namespace VoiceStation.AppHelper.Abstract
{
    public interface IProcessAcessor
    {
        Process CurrentProcess { get; }
        Process[] Processes { get; }
        Process GetProcessByName(string name);
    }
}