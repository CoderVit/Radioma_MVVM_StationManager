﻿using VoiceStation.AppHelper.Abstract;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Handler.Abstract;
using VoiceStation.MVVM.Models.Windows;
using VoiceStation.SettingHelper.Abstract;

namespace VoiceStation.AppCreator.Builders.ViewModels
{
    public class MainWindowVmBuilder
    {
        private IMediator m_mediator;
        private INamesHelper m_namesHelper;
        private IPttHandler m_pttHandler;
        private IAppManager m_appManager;

        #region Builder Methods

        public MainWindowVmBuilder WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public MainWindowVmBuilder WithNamesHelper(INamesHelper namesHelper)
        {
            m_namesHelper = namesHelper;
            return this;
        }

        public MainWindowVmBuilder WithPttHandler(IPttHandler pttHandler)
        {
            m_pttHandler = pttHandler;
            return this;
        }

        public MainWindowVmBuilder WithAppManager(IAppManager appManager)
        {
            m_appManager = appManager;
            return this;
        }

        #endregion Builder Methods

        public MainWindowViewModel Build()
        {
            return new MainWindowViewModel(m_pttHandler, m_appManager)
            {
                StationNames = m_namesHelper.GetNames()
            };
        }
    }
}