﻿using VoiceStation.Convertors.Abstract;
using VoiceStation.Convertors.Concrete;

namespace VoiceStation.AppCreator.Creators
{
    public class WaveConvertorCreater
    {
        public ITypeConvertor Create()
        {
            return new WaveConverter();
        }
    }
}
