﻿using System;
using VoiceStation.NetworkClients.Abstractions;
using VoiceStation.Package.Abstractions;

namespace VoiceStation.NetworkClients.Concrete
{
    public class WaveSender : IWaveSender
    {
        private readonly IClient m_client;
        private readonly IPackageCreator m_packageCreator;

        public bool IsCanSend { get; set; }
        public bool IsCanInitiate { get; set; }

        public WaveSender(IClient client, IPackageCreator packageCreator)
        {
            if (client == null) throw new ArgumentNullException("client");
            m_client = client;
            if (packageCreator == null) throw new ArgumentNullException("packageCreator");
            m_packageCreator = packageCreator;
        }

        public void Send(byte[] data)
        {
            m_client.Send(m_packageCreator.GetPackage(data));
        }

        public void Stop()
        {
            m_packageCreator.ChangeRandom();
        }
    }
}