﻿using System;
using System.IO;
using System.Web.Script.Serialization;
using VoiceStation.Config.Abstract;
using VoiceStation.Config.Concrete;
using VoiceStation.Config.Models;

namespace VoiceStation.AppCreator.Builders
{
    public class ConfigHelperBuilder
    {
        private string m_configPath;
        private JavaScriptSerializer m_serializer;

        #region Builder Methods

        public ConfigHelperBuilder WithConfigPath(string fileName)
        {
            m_configPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            return this;
        }

        public ConfigHelperBuilder WithSerializer(JavaScriptSerializer serializer)
        {
            m_serializer = serializer;
            return this;
        }

        public ConfigHelperBuilder WithConvertors(StationConverter converter)
        {
            m_serializer.RegisterConverters(new[] { converter });
            return this;
        }

        #endregion Builder Methods

        public IConfigHelper Build()
        {
            return new ConfigHelper(m_configPath, m_serializer);
        }
    }
}
