﻿using System;
using System.Windows.Input;
using VoiceStation.AppHelper.Abstract;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models
{
    /// <summary>
    /// Класс модели для вызова программы RrcComplex
    /// </summary>
    public class InvokeRrcViewModel : ViewModel
    {
        private readonly IProcessInvoker m_processInvoker;

        public InvokeRrcViewModel(IProcessInvoker processInvoker)
        {
            if (processInvoker == null) throw new ArgumentNullException("processInvoker");
            m_processInvoker = processInvoker;
        }

        private void InvokeRrc()
        {
            try
            {
                m_processInvoker.Invoke();
            }
            catch (Exception)
            {
                IsInvokePossible = false;
            }
        }

        #region Commands

        public ICommand InvokeRrcCommand { get { return new StationCommand(InvokeRrc); } }

        #endregion Commands

        #region Properties

        private bool m_isInvokePossible;
        public bool IsInvokePossible
        {
            get { return m_isInvokePossible; }
            set
            {
                m_isInvokePossible = value;
                RaisePropertyChanged("IsInvokePossible");
            }
        }

        #endregion Properties
    }
}