﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;

namespace VoiceStation.Config.Models
{
    public class StationConverter : JavaScriptConverter
    {
        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            //TODO ini from foreach and set property
            return new StationConfig
            {
                RrcPath = serializer.ConvertToType<string>(dictionary["rrcPath"]),
                ReceiveTimeOut = serializer.ConvertToType<ushort>(dictionary["recTimeOut"]),
                DestinationPorts = serializer.ConvertToType<ushort[]>(dictionary["destPorts"]),
                Addresses = serializer.ConvertToType<string[]>(dictionary["addresses"]).Select(IPAddress.Parse).ToArray(),
                ChanelIds = serializer.ConvertToType<ushort[]>(dictionary["channelIds"])
            };
        }

        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            throw new NotSupportedException();
        }

        public override IEnumerable<Type> SupportedTypes
        {
            get { return new[] { typeof(StationConfig) }; }
        }
    }
}