﻿using System;
using VoiceStation.AudioHelper.Models;

namespace VoiceStation.AudioHelper.Abstractions
{
    public interface IListenHelper
    {
        event Action<ListenStatus> OnUpdateListenStatus;
        event Action<ushort> OnOutValueUpdated;

        WaveOutMode WaveOutMode { get; set; }
        double ChannelVolumeLevel { set; }

        void HandleAudio(byte[] data);

        void ListenStart();
        void Play();
        void Stop();
    }
}