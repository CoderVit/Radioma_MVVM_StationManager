﻿using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;
using VoiceStation.NetworkClients.Concrete;

namespace VoiceStation.AppCreator.Builders.UdpClients
{
    public class CommonClientBuilder
    {
        private ControlClientBuilder m_controlClientBuilder;

        #region Builder Methods

        public CommonClientBuilder WithClientBuilder(ControlClientBuilder controlClientBuilder)
        {
            m_controlClientBuilder = controlClientBuilder;
            return this;
        }

        #endregion Builder Methods

        public ICommonClient Build(ChannelNumber channel)
        {
            return new CommonClient(m_controlClientBuilder.Build(channel));
        }
    }
}