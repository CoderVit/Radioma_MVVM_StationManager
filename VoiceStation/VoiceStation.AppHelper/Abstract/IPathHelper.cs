﻿namespace VoiceStation.AppHelper.Abstract
{
    public interface IPathHelper
    {
        string PureFileName { get; }
        string FullPath { get; }
        bool IsFileExist { get; }
    }
}
