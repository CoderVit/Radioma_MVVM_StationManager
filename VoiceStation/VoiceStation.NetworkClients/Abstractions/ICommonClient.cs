﻿using System;

namespace VoiceStation.NetworkClients.Abstractions
{
    public interface ICommonClient
    {
        event Action<bool> OnStatusUpdate;
        event Action<byte[]> OnAudioReceived;
        event Action<bool> OnPttStatusUpdate;

        void PingStart();
        void PingStop();
        void RxStart();
    }
}