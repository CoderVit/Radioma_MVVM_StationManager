﻿namespace VoiceStation.AudioHelper.Abstractions
{
    public interface IVolumeManager
    {
        bool IsMute { get; set; }
        float SystemVolume { get; set; }
    }
}