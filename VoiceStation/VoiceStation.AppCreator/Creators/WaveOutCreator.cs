﻿using NAudio.Wave;

namespace VoiceStation.AppCreator.Creators
{
    public class WaveOutCreator
    {
        private byte m_defaultDeviceNumber;

        #region Builder Methods

        public WaveOutCreator WithDeviceNumber(byte number)
        {
            m_defaultDeviceNumber = number;
            return this;
        }

        #endregion Builder Methods

        public WaveOut Create()
        {
            return new WaveOut
            {
                DeviceNumber = m_defaultDeviceNumber
            };
        }
    }
}