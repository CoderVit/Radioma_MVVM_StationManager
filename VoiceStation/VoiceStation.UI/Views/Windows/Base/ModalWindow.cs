﻿using System.Windows;
using System.Windows.Input;
using VoiceStation.MVVM.Commands;

namespace VoiceStation.UI.Views.Windows.Base
{
    /// <summary>
    /// Базовый класс для модальных окон
    /// Включает стандартные свойства инициализации и команды закрыть окно
    /// </summary>
    public class ModalWindow : Window
    {
        /// <summary>
        /// конструктор модального окна
        /// </summary>
        public ModalWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            ShowInTaskbar = false;
            Owner = Application.Current.MainWindow;
            //Привязка нажатия клавиши для закрытия окна
            InputBindings.Add(new KeyBinding
            {
                Key = Key.Escape,
                Command = CloseWindowCommand
            });
        }
        /// <summary>
        /// делегат закрытия окна, обнуление дата-контекста и вызов метода закрытия окна Window.Close
        /// </summary>
        private void CloseWindow()
        {
            DataContext = null;
            Close();
        }

        #region Commands

        public ICommand CloseWindowCommand { get { return new StationCommand(CloseWindow); } }

        #endregion Commands

        #region Properties

        public string Header { get; internal set; }

        #endregion Properties
    }
}