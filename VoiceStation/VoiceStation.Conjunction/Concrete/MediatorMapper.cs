﻿using System;
using System.Collections.Generic;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;

namespace VoiceStation.Conjunction.Concrete
{
    public class MediatorMapper : IMediatorMap
    {
        private readonly Dictionary<StationMessage, List<Action>> m_map;

        public MediatorMapper(Dictionary<StationMessage, List<Action>> map)
        {
            if (map == null) throw new ArgumentNullException("map");
            m_map = map;
        }

        public void AddAction(StationMessage message, Action action)
        {
            if (!m_map.ContainsKey(message))
            {
                m_map[message] = new List<Action>();
            }

            m_map[message].Add(action);
        }

        public List<Action> GetActions(StationMessage message)
        {
            return !m_map.ContainsKey(message) ? null : m_map[message];
        }
    }
}