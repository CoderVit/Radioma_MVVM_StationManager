﻿using System.Windows;
using System.Windows.Input;

namespace VoiceStation.UI.Behaviours
{
    internal class MouseBehaviour
    {
        #region Mouse Down

        private static readonly DependencyProperty srm_MouseDownCommandProperty = DependencyProperty.RegisterAttached(
            "MouseDownCommand",
            typeof(ICommand),
            typeof(MouseBehaviour),
            new FrameworkPropertyMetadata(
                (o, args) => ((FrameworkElement)o).PreviewMouseLeftButtonDown +=
                    (sender, e) => ((ICommand)((FrameworkElement)sender).GetValue(srm_MouseDownCommandProperty)).Execute(e)));

        public static void SetMouseDownCommand(UIElement element, ICommand value)
        {
            element.SetValue(srm_MouseDownCommandProperty, value);
        }

        #endregion Mouse Down

        #region Mouse Up

        private static readonly DependencyProperty srm_MouseUpCommandProperty = DependencyProperty.RegisterAttached(
           "MouseUpCommand",
           typeof(ICommand),
           typeof(MouseBehaviour),
           new FrameworkPropertyMetadata(
               (o, args) => ((FrameworkElement)o).PreviewMouseLeftButtonUp +=
                   (sender, e) => ((ICommand)((FrameworkElement)sender).GetValue(srm_MouseUpCommandProperty)).Execute(e)));

        public static void SetMouseUpCommand(UIElement element, ICommand value)
        {
            element.SetValue(srm_MouseUpCommandProperty, value);
        }

        #endregion Mouse Up

    }
}