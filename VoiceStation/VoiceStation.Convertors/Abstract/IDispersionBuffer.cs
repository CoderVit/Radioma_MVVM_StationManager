﻿namespace VoiceStation.Convertors.Abstract
{
    public interface IDispersionBuffer
    {
        void Push(ushort dispersion);
        ushort GetMiddle();
    }
}
