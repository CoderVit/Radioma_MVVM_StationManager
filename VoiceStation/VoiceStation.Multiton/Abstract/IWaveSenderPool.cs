﻿using VoiceStation.Multiton.Models;

namespace VoiceStation.Multiton.Abstract
{
    public interface IWaveSenderPool
    {
        bool AnyActive { get; }
        void Add(ChannelNumber channel);
        void Release(ChannelNumber channel);
        void Send(byte[] data);
    }
}