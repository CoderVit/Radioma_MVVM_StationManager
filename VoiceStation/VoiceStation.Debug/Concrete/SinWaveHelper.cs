﻿using System;
using VoiceStation.Engineer.SinHelper.Abstract;

namespace VoiceStation.Engineer.SinHelper.Concrete
{
    public class SinWaveHelper : ISinWaveHelper
    {
        private readonly ISinStorage m_sinStorage;

        public SinWaveHelper(ISinStorage sinStorage)
        {
            if (sinStorage == null) throw new ArgumentNullException("sinStorage");
            m_sinStorage = sinStorage;
        }

        public short[] GetPackage()
        {
            var sinPackage = new short[160];

            for (var packageIterator = 0; packageIterator < sinPackage.Length; packageIterator++)
            {
                sinPackage[packageIterator] = m_sinStorage.GetPoint();
            }
            return sinPackage;
        }
    }
}