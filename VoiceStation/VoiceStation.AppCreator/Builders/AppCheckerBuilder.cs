﻿using VoiceStation.AppHelper.Abstract;
using VoiceStation.AppHelper.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class AppCheckerBuilder
    {
        private IProcessAcessor m_processAcessor;
        private IDllImportHelper m_dllImportHelper;

        #region Builder Methods

        public AppCheckerBuilder WithProcessAcessor(IProcessAcessor processAcessor)
        {
            m_processAcessor = processAcessor;
            return this;
        }

        public AppCheckerBuilder WithDllImportHelper(IDllImportHelper dllImportHelper)
        {
            m_dllImportHelper = dllImportHelper;
            return this;
        }

        #endregion Builder Methods

        public IAppChecker Build()
        {
            return new AppChecker(m_processAcessor, m_dllImportHelper);
        }
    }
}