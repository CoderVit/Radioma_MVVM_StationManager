﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using VoiceStation.AppHelper.Abstract;
using VoiceStation.Handler.Abstract;
using VoiceStation.Handler.Models;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models.Windows
{
    /// <summary>
    /// Модель главного окна приложения, с событиями нажатия/отжатия мыши и клаивиатуры.
    /// </summary>
    public class MainWindowViewModel : ViewModel
    {
        private readonly IPttHandler m_pttHandler;
        private readonly IAppManager m_appManager;

        public event Action OnSettingModeChange;

        public MainWindowViewModel(IPttHandler pttHandler, IAppManager appManager)
        {
            if (pttHandler == null) throw new ArgumentNullException("pttHandler");
            m_pttHandler = pttHandler;
            if (appManager == null) throw new ArgumentNullException("appManager");
            m_appManager = appManager;
        }
        /// <summary>
        /// Проверка события KeyUp, в диапазоне D1...D4 и Space
        /// </summary>
        /// <param name="key">поле класса события соответствующее отжатой клавише</param>
        private void KeuUpHandle(Key key)
        {
            if ((key < Key.D1 || key > Key.D4) && key != Key.Space) return;
            m_pttHandler.KeyBoardOff((PttStatement)key);
        }

        #region Commands

        public ICommand KeyPressedCommand { get { return new StationCommand(key => m_pttHandler.KeyBoardOn((PttStatement)key)); } }

        public ICommand KeyUpCommand { get { return new StationCommand(key => KeuUpHandle(((KeyEventArgs)key).Key)); } }

        public ICommand LeftMouseUpCommand { get { return new StationCommand(m_pttHandler.MouseOff); } }

        public ICommand CloseAppCommand { get { return new StationCommand(m_appManager.ShutDown); } }

        public ICommand ToggleModeCommand { get { return new StationCommand(() => OnSettingModeChange()); } }

        #endregion Commands

        #region Properties

        private IEnumerable<string> m_stationNames;
        public IEnumerable<string> StationNames
        {
            get { return m_stationNames; }
            set
            {
                m_stationNames = value;
                RaisePropertyChanged("StationNames");
            }
        }

        #endregion Properties
    }
}