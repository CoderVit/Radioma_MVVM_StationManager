﻿using System;
using System.Diagnostics;
using VoiceStation.AppHelper.Abstract;

namespace VoiceStation.AppHelper.Concrete
{
    public class ProcessInvoker : IProcessInvoker
    {
        private readonly IAppChecker m_appChecker;
        private readonly IPathHelper m_pathHelper;

        public ProcessInvoker(IAppChecker appChecker, IPathHelper pathHelper)
        {
            if (appChecker == null) throw new ArgumentNullException("appChecker");
            m_appChecker = appChecker;
            if (pathHelper == null) throw new ArgumentNullException("pathHelper");
            m_pathHelper = pathHelper;
        }

        public void Invoke()
        {
            if (!m_appChecker.IsAppRunning(m_pathHelper.PureFileName)) Process.Start(m_pathHelper.FullPath);
        }
    }
}