﻿using VoiceStation.Multiton.Models;

namespace VoiceStation.Micro.Abstractions
{
    public interface ISinHelper
    {
        void Add(ChannelNumber waveSenderType);
        void Remove(ChannelNumber waveSenderType);
        void Release();
    }
}