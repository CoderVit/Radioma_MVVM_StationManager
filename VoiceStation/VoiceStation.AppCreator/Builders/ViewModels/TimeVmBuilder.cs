﻿using VoiceStation.Common.Abstract;
using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Builders.ViewModels
{
    public class TimeVmBuilder
    {
        private ITime m_time;

        #region Builders Method

        public TimeVmBuilder WithTimeAdapter(ITime time)
        {
            m_time = time;
            return this;
        }

        #endregion Builders Method

        public TimeViewModel Build()
        {
            return new TimeViewModel(m_time) { CurrentTime = m_time.Now }.ModelRun();
        }
    }
}