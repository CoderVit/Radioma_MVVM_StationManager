﻿using System;
using VoiceStation.Conjunction.Models;

namespace VoiceStation.Conjunction.Abstractions
{
    public interface IMediator
    {
        void Register(StationMessage message, Action callback);
        void RegisterStartPtt(StationMessage message, Action callback);
        void RegisterStopPtt(StationMessage message, Action callback);
        void Notify(StationMessage message);
    }
}