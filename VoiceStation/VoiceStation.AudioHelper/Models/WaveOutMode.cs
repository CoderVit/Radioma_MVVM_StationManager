﻿namespace VoiceStation.AudioHelper.Models
{
    public enum WaveOutMode
    {
        WAVE_OUT_MODE_OFF,
        WAVE_OUT_MODE_PLAY,
        WAVE_OUT_MODE_PLAY_INTERCOM
    }
}