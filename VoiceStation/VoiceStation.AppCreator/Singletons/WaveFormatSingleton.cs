﻿using NAudio.Wave;

namespace VoiceStation.AppCreator.Singletons
{
    public class WaveFormatSingleton
    {
        private const ushort RATE = 8000;
        private const byte BITS = 16;
        private const byte CHANNELS_NUMBER = 1;

        private WaveFormat m_waveFormat;

        public WaveFormat Instance { get { return m_waveFormat ?? (m_waveFormat = new WaveFormat(RATE, BITS, CHANNELS_NUMBER)); } }
    }
}