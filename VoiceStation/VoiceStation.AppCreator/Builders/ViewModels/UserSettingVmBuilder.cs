﻿using VoiceStation.AppCreator.Creators;
using VoiceStation.MVVM.Models.Windows;

namespace VoiceStation.AppCreator.Builders.ViewModels
{
    public class UserSettingVmBuilder
    {
        private WaveInCreator m_waveInCreator;
        private DispersionHelperBuilder m_dispersionHelperBuilder;

        #region Builder Methods

        public UserSettingVmBuilder WithWaveInCreator(WaveInCreator waveInCreator)
        {
            m_waveInCreator = waveInCreator;
            return this;
        }

        public UserSettingVmBuilder WithDispersionHelperBuilder(DispersionHelperBuilder dispersionHelperBuilder)
        {
            m_dispersionHelperBuilder = dispersionHelperBuilder;
            return this;
        }

        #endregion Builder Methods
        /// <summary>
        /// Создание модели, подписка на событие получения потока с микрофона.
        /// Обработка через дисперсионный буфер.
        /// Присвоение значение для индикатора.
        /// </summary>
        /// <returns>Модель представления, со всеми подписями на события и предварительной инициализацией</returns>
        public UserSettingViewModel Build()
        {
            var waveIn = m_waveInCreator.CreateWaveIn();
            var dispersionBuffer = m_dispersionHelperBuilder.Build();
            var vm = new UserSettingViewModel();

            vm.OnWindowClose += () =>
            {
                waveIn.StopRecording();
                waveIn.Dispose();
            };
            waveIn.DataAvailable += (sender, args) => vm.MicroLevel = dispersionBuffer.Compute(args.Buffer);
            waveIn.StartRecording();

            return vm;
        }
    }
}