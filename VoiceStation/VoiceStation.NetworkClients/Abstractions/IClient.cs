﻿namespace VoiceStation.NetworkClients.Abstractions
{
    public interface IClient
    {
        void Send(byte[] inputData);
        byte[] Receive();
    }
}