﻿using VoiceStation.ExpandedMode.Abstract;
using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Singletons.ViewModels
{
    public class ExpandedModeVmSingleton
    {
        private IRrcHelper m_rrcHelper;

        private ExpandedModeViewModel m_expandedModeViewModel;

        #region Builder Methods

        public ExpandedModeVmSingleton WithRrcHelper(IRrcHelper rrcHelper)
        {
            m_rrcHelper = rrcHelper;
            return this;
        }

        #endregion Builder Methods

        public ExpandedModeViewModel Instance
        {
            get
            {
                return m_expandedModeViewModel ?? (m_expandedModeViewModel = new ExpandedModeViewModel(m_rrcHelper));
            }
        }
    }
}