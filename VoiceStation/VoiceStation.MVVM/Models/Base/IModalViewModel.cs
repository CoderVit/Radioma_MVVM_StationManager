﻿using System;

namespace VoiceStation.MVVM.Models.Base
{
    //Базовый интерфейс с единым событием нажатия закрыти текущего модального окна
    public interface IModalViewModel
    {
        event Action OnWindowClose;
    }
}