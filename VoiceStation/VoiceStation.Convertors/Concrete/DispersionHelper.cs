﻿using System;
using VoiceStation.Convertors.Abstract;

namespace VoiceStation.Convertors.Concrete
{
    public class DispersionHelper : IDispersionHelper
    {
        private readonly IDispersionBuffer m_buffer;

        public DispersionHelper(IDispersionBuffer buffer)
        {
            if (buffer == null) throw new ArgumentNullException("buffer");
            m_buffer = buffer;
        }

        public ushort Compute(byte[] waves)
        {
            var elementsSum = 0;
            var elementsMultiplicable = 0;

            for (var iterator = 0; iterator < waves.Length; )
            {
                var val = (short)(waves[iterator++] << 8 | waves[iterator++]);

                var str = val >> 8;
                elementsSum += str;
                elementsMultiplicable += str * str;
            }

            var m = elementsSum / 160;
            m_buffer.Push((ushort)(elementsMultiplicable / 160 - m * m));
            return m_buffer.GetMiddle();
        }
    }
}