﻿namespace VoiceStation.Convertors.Abstract
{
    public interface ILogarithmConvertor
    {
        double Convert(double value);
    }
}