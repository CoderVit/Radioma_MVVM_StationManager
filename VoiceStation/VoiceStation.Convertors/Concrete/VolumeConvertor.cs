﻿using VoiceStation.Convertors.Abstract;

namespace VoiceStation.Convertors.Concrete
{
    public class VolumeConvertor : IVolumeConvertor
    {
        private const byte COFFICIENT = 255;

        public byte FromSystemVolume(float value)
        {
            return (byte)(value * COFFICIENT);
        }

        public float ToSystemVolume(byte value)
        {
            return (float)value / COFFICIENT;
        }
    }
}