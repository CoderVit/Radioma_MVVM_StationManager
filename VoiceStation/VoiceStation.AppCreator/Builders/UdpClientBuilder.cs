﻿using System;
using System.Net.Sockets;

namespace VoiceStation.AppCreator.Builders
{
    public class UdpClientBuilder
    {
        private readonly ushort m_timeOut;

        public UdpClientBuilder(ushort timeOut)
        {
            if (timeOut == 0) throw new Exception("timeOut");
            m_timeOut = timeOut;
        }

        public UdpClient Build()
        {
            return new UdpClient { Client = { SendTimeout = 100, ReceiveTimeout = m_timeOut } };
        }
    }
}