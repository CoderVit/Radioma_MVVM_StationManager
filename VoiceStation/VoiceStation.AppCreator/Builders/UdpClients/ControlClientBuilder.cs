﻿using System.Net.Sockets;
using VoiceStation.AppCreator.Builders.EndPoint;
using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;
using VoiceStation.NetworkClients.Concrete;

namespace VoiceStation.AppCreator.Builders.UdpClients
{
    public class ControlClientBuilder
    {
        private UdpClientBuilder m_udpClientCreator;
        private LocalEndPointBuilder m_localEndPointBuilder;
        private RemoteEndPointBuilder m_remoteEndPointBuilder;

        #region Builder Methods

        public ControlClientBuilder WithUdpCreator(UdpClientBuilder udpBuilder)
        {
            m_udpClientCreator = udpBuilder;
            return this;
        }

        public ControlClientBuilder WithLocalEndPointBuilder(LocalEndPointBuilder localEndPointBuilder)
        {
            m_localEndPointBuilder = localEndPointBuilder;
            return this;
        }

        public ControlClientBuilder WithRemoteEndPointCreator(RemoteEndPointBuilder remoteEndPointBuilder)
        {
            m_remoteEndPointBuilder = remoteEndPointBuilder;
            return this;
        }

        #endregion Builder Methods

        public IClient Build(ChannelNumber channel)
        {
            var client = m_udpClientCreator.Build();
            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            client.Client.Bind(m_localEndPointBuilder.Build(channel));
            client.Connect(m_remoteEndPointBuilder.Build(channel));
            return new Client(client);
        }
    }
}