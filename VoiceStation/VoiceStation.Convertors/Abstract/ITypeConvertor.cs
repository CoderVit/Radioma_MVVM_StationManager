﻿namespace VoiceStation.Convertors.Abstract
{
    public interface ITypeConvertor
    {
        short[] ToShorts(byte[] bytes);
    }
}