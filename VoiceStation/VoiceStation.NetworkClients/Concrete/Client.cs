﻿using System;
using System.Net;
using System.Net.Sockets;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.NetworkClients.Concrete
{
    public class Client : IClient
    {
        private readonly UdpClient m_udpClient;

        public Client(UdpClient client)
        {
            if (client == null) throw new ArgumentNullException("client");
            m_udpClient = client;
        }

        public void Send(byte[] inputData)
        {
            m_udpClient.Send(inputData, inputData.Length);
        }

        public byte[] Receive()
        {
            IPEndPoint remoteIpEndPoint = null;
            return m_udpClient.Receive(ref remoteIpEndPoint);
        }
    }
}