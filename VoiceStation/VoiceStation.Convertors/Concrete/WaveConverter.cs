﻿using VoiceStation.Convertors.Abstract;

namespace VoiceStation.Convertors.Concrete
{
    public class WaveConverter : ITypeConvertor
    {
        private const byte ARRAY_SIZE = 160;
        private const byte SMALL_SHIFT = 8;

        public short[] ToShorts(byte[] bytes)
        {
            var shortArray = new short[ARRAY_SIZE];

            for (ushort bufferIterator = 0, shortIterator = 0; bufferIterator < bytes.Length; shortIterator++)
            {
                shortArray[shortIterator] = (short)(bytes[bufferIterator++] | (bytes[bufferIterator++] << SMALL_SHIFT));
            }
            return shortArray;
        }
    }
}