﻿using System;
using System.Reflection;
using System.Windows.Threading;
using VoiceStation.ExpandedMode.Abstract;

namespace VoiceStation.ExpandedMode
{
    public class ChannelGuideHelper : IChannelGuideHelper
    {
        public event Action<decimal> OnFrequenceUpdate;
        public bool IsConnected { get { return !m_channelsGuide.Radio.Connecting && !m_channelsGuide.Radio.Connected && !m_channelsGuide.Radio.Detected; } }

        private readonly string m_connectionString;
        private dynamic m_channelsGuide;

        public ChannelGuideHelper(string connectionString)
        {
            if (connectionString == null) throw new ArgumentNullException("connectionString");
            m_connectionString = connectionString;
        }

        public void ChannelGuideStart(Dispatcher dispatcher)
        {
            var assembly = Assembly.Load("RRComplexLib, Version=11.0.0.0, Culture=neutral, PublicKeyToken=22c7d037cd326e65, processorArchitecture=MSIL");
            m_channelsGuide = Activator.CreateInstance(assembly.GetType("RRComplexLib.ChannelsGuide"), m_connectionString, dispatcher);
            m_channelsGuide.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler((sender, args) => { if (args.PropertyName == "CurrentFrequency") OnFrequenceUpdate(m_channelsGuide.CurrentFrequency); });
            m_channelsGuide.Radio.Connect();
        }

        public void Reconnect()
        {
            try
            {
                m_channelsGuide.Radio.Connect();
            }
            catch (Exception) { }
        }
    }
}