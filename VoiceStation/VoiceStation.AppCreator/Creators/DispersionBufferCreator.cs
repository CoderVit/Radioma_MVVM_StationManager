﻿using VoiceStation.Convertors.Abstract;
using VoiceStation.Convertors.Concrete;

namespace VoiceStation.AppCreator.Creators
{
    public class DispersionBufferCreator
    {
        public IDispersionBuffer Create()
        {
            return new DispersionBuffer();
        }
    }
}