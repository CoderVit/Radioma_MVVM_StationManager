﻿using VoiceStation.Convertors.Concrete;

namespace VoiceStation.AppCreator.Creators
{
    public class VolumeConvertorCreator
    {
        public VolumeConvertor Create()
        {
            return new VolumeConvertor();
        }
    }
}