﻿using System.Collections.Generic;
using VoiceStation.Codec.Models;

namespace VoiceStation.Codec.Abstraction
{
    public interface IG711
    {
        Linear UlawToLinear(IEnumerable<byte> waveBuffer, double volumeLevel);
        byte[] LinearToUlaw(short[] pcmValues);
    }
}