﻿namespace VoiceStation.SettingHelper.Abstract
{
    public interface IEncryptManager
    {
        string Encrypt(string data);
        string Decrypt(string data);
    }
}
