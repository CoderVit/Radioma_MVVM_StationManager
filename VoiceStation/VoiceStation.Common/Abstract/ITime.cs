﻿using System;

namespace VoiceStation.Common.Abstract
{
    public interface ITime
    {
        DateTime Now { get; }
        DateTime Default { get; }
    }
}
