﻿namespace VoiceStation.ExpandedMode.Abstract
{
    public interface IRrcHelper
    {
        IChannelGuideHelper this[byte index] { set; }
        void TryToEnable();
        void RealTimeWorkStart();
    }
}