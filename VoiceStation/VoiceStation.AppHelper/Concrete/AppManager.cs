﻿using System;
using System.Windows;
using VoiceStation.AppHelper.Abstract;

namespace VoiceStation.AppHelper.Concrete
{
    public class AppManager : IAppManager
    {
        public void ShowError(string message)
        {
            MessageBox.Show(message, "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void ShutDown()
        {
            Environment.Exit(1);
        }
    }
}