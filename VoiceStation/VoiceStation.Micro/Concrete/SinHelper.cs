﻿using NAudio.Wave;
using System;
using VoiceStation.Codec.Abstraction;
using VoiceStation.Engineer.SinHelper.Abstract;
using VoiceStation.Micro.Abstractions;
using VoiceStation.Multiton.Abstract;
using VoiceStation.Multiton.Models;

namespace VoiceStation.Micro.Concrete
{
    public class SinHelper : IMicroTest
    {
        private readonly IWaveIn m_waveIn;
        private readonly IG711 m_codec;
        private readonly ISinWaveHelper m_sinWaveHelper;
        private readonly IWaveSenderPool m_waveSenderPool;

        public SinHelper(IWaveIn waveIn, IG711 codec, ISinWaveHelper sinWaveHelper, IWaveSenderPool waveSenderPool)
        {
            if (waveIn == null) throw new ArgumentNullException("waveIn");
            m_waveIn = waveIn;
            if (codec == null) throw new ArgumentNullException("codec");
            m_codec = codec;
            if (sinWaveHelper == null) throw new ArgumentNullException("sinWaveHelper");
            m_sinWaveHelper = sinWaveHelper;
            if (waveSenderPool == null) throw new ArgumentNullException("waveSenderPool");
            m_waveSenderPool = waveSenderPool;
        }

        private void WaveInOnDataAvailable(object sender, WaveInEventArgs waveInEventArgs)
        {
            var package = m_codec.LinearToUlaw(m_sinWaveHelper.GetPackage());
            m_waveSenderPool.Send(package);
        }

        public void Add(ChannelNumber channel)
        {
            if (!m_waveSenderPool.AnyActive) Start();
            m_waveSenderPool.Add(channel);
        }

        public void Remove(ChannelNumber channel)
        {
            m_waveSenderPool.Release(channel);
            if (!m_waveSenderPool.AnyActive) Stop();
        }

        private void Start()
        {
            m_waveIn.DataAvailable += WaveInOnDataAvailable;
            m_waveIn.StartRecording();
        }

        private void Stop()
        {
            m_waveIn.StopRecording();
        }

        public void Release()
        {
            Stop();
            m_waveIn.Dispose();
        }
    }
}