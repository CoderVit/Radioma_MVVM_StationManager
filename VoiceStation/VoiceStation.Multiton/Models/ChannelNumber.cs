﻿namespace VoiceStation.Multiton.Models
{
    public enum ChannelNumber : byte
    {
        CHANNEL_NUMBER_FIRST,
        CHANNEL_NUMBER_SECOND,
        CHANNEL_NUMBER_THIRD,
        CHANNEL_NUMBER_FOURTH
    }
}