﻿using VoiceStation.Common.Abstract;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Handler.Abstract;
using VoiceStation.Handler.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class PttHandlerBuilder
    {
        private IMediator m_mediator;
        private ITime m_time;

        #region Builder Methods

        public PttHandlerBuilder WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public PttHandlerBuilder WithTimeAdapter(ITime time)
        {
            m_time = time;
            return this;
        }

        #endregion Builder Methods

        public IPttHandler Build()
        {
            return new PttHandler(m_mediator, m_time);
        }
    }
}