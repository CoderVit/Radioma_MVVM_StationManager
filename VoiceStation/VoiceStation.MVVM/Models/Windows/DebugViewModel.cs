﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using VoiceStation.Micro.Abstractions;
using VoiceStation.Multiton.Models;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models.Windows
{
    public class DebugViewModel : ViewModel, IModalViewModel
    {
        private readonly IMicroTest m_microTest;

        public event Action OnWindowClose;

        public DebugViewModel(IMicroTest microTest)
            : this()
        {
            if (microTest == null) throw new ArgumentNullException("microTest");
            m_microTest = microTest;
        }

        private DebugViewModel()
        {
            m_lostPackages = new ObservableCollection<int> { 0, 0, 0, 0 };
        }

        private void StartSin(ChannelNumber channel)
        {
            m_microTest.Add(channel);
        }

        private void StopSin(ChannelNumber channel)
        {
            m_microTest.Remove(channel);
        }

        public void Close()
        {
            OnWindowClose();
            m_microTest.Release();
        }

        #region Commands

        public ICommand CloseCommand { get { return new StationCommand(Close); } }

        public ICommand FirstSinWaveCommand { get { return new StationCommand(isChecked => ((bool)isChecked ? (Action<ChannelNumber>)StartSin : StopSin)(ChannelNumber.CHANNEL_NUMBER_FIRST)); } }

        public ICommand SecondSinWaveCommand { get { return new StationCommand(isChecked => ((bool)isChecked ? (Action<ChannelNumber>)StartSin : StopSin)(ChannelNumber.CHANNEL_NUMBER_SECOND)); } }

        public ICommand ThirdSinWaveCommand { get { return new StationCommand(isChecked => ((bool)isChecked ? (Action<ChannelNumber>)StartSin : StopSin)(ChannelNumber.CHANNEL_NUMBER_THIRD)); } }

        public ICommand FourthSinWaveCommand { get { return new StationCommand(isChecked => ((bool)isChecked ? (Action<ChannelNumber>)StartSin : StopSin)(ChannelNumber.CHANNEL_NUMBER_FOURTH)); } }

        #endregion Commands

        #region Properties

        private ObservableCollection<int> m_lostPackages;
        public ObservableCollection<int> LostPackages
        {
            get { return m_lostPackages; }
            set
            {
                m_lostPackages = value;
                RaisePropertyChanged("LostPackages");
            }
        }

        #endregion Properties
    }
}