﻿using VoiceStation.AppCreator.Builders.Sin;
using VoiceStation.AppCreator.Creators;
using VoiceStation.Micro.Abstractions;
using VoiceStation.Micro.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class SinHelperBuilder
    {
        private WaveInCreator m_waveInCreator;
        private CodecCreator m_codecCreator;
        private SinWaveHelperBuilder m_sinWaveHelperBuilder;
        private WaveSenderPoolBuilder m_waveSenderPoolBuilder;

        #region Builder Methods

        public SinHelperBuilder WithWaveInCreator(WaveInCreator waveInCreator)
        {
            m_waveInCreator = waveInCreator;
            return this;
        }

        public SinHelperBuilder WithCodecCreator(CodecCreator codecCreator)
        {
            m_codecCreator = codecCreator;
            return this;
        }

        public SinHelperBuilder WithSinWaveHelperBuilder(SinWaveHelperBuilder sinWaveHelperBuilder)
        {
            m_sinWaveHelperBuilder = sinWaveHelperBuilder;
            return this;
        }

        public SinHelperBuilder WithWaveSenderPoolBuilder(WaveSenderPoolBuilder waveSenderPoolBuilder)
        {
            m_waveSenderPoolBuilder = waveSenderPoolBuilder;
            return this;
        }

        #endregion Builder Methods

        public IMicroTest Build()
        {
            return new SinHelper(
                m_waveInCreator.CreateWaveIn(),
                m_codecCreator.Create(),
                m_sinWaveHelperBuilder.Build(),
                m_waveSenderPoolBuilder.Build());
        }
    }
}