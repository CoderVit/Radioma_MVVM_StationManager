﻿using NAudio.CoreAudioApi;
using System;
using VoiceStation.AudioHelper.Abstractions;

namespace VoiceStation.AudioHelper.Concrete
{
    public class SystemVolumeManager : IVolumeManager
    {
        private readonly MMDevice m_mmDevice;

        public SystemVolumeManager(MMDevice mmDevice)
        {
            if (mmDevice == null) throw new ArgumentNullException("mmDevice");
            m_mmDevice = mmDevice;
        }

        public bool IsMute
        {
            get { return m_mmDevice.AudioEndpointVolume.Mute; }
            set { m_mmDevice.AudioEndpointVolume.Mute = value; }
        }

        public float SystemVolume
        {
            get { return m_mmDevice.AudioEndpointVolume.MasterVolumeLevelScalar; }
            set { m_mmDevice.AudioEndpointVolume.MasterVolumeLevelScalar = value; }
        }
    }
}