﻿using NAudio.Wave;
using System;
using VoiceStation.Codec.Abstraction;
using VoiceStation.Convertors.Abstract;
using VoiceStation.Micro.Abstractions;
using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.Micro.Concrete
{
    public class MicroHelper : IMicroHelper
    {
        private const byte ARRAY_SIZE = 4;
        private volatile IWaveSender[] m_senders = new IWaveSender[ARRAY_SIZE];

        public MicroHelper(IWaveIn waveIn, IG711 codec, ITypeConvertor typeConvertor)
        {
            if (waveIn == null) throw new ArgumentNullException("waveIn");
            if (codec == null) throw new ArgumentNullException("codec");
            if (typeConvertor == null) throw new ArgumentNullException("typeConvertor");
            waveIn.DataAvailable += (sender, args) =>
            {
                var package = codec.LinearToUlaw(typeConvertor.ToShorts(args.Buffer));
                for (byte sendersIterator = 0; sendersIterator < ARRAY_SIZE; sendersIterator++)
                {
                    var waveSender = m_senders[sendersIterator];
                    //Debug.WriteLine("WaveSender IsActive to send {0}", waveSender.IsCanSend);
                    if (waveSender.IsCanSend) waveSender.Send(package);
                }
            };
            waveIn.StartRecording();
        }

        public IWaveSender this[byte index]
        {
            set
            {
                m_senders[index] = value;
            }
        }

        public void PttStart(ChannelNumber channel)
        {
            //Debug.WriteLine("MicroHelper PttStart {0}, any senders has {1}", channel, m_senders.Any(s => s.IsCanInitiate));

            //if (!m_senders.Any(s => s.IsCanInitiate))
            //{
            //    Debug.WriteLine("MicroHelper PttStart m_waveIn.StartRecording()");
            //}
            var sender = m_senders[(byte)channel];
            sender.IsCanInitiate = true;
            sender.IsCanSend = true;
        }

        public void PttStop(ChannelNumber channel)
        {
            //Debug.WriteLine("MicroHelper PttStop {0}", channel);
            var sender = m_senders[(byte)channel];
            sender.IsCanInitiate = false;
            sender.IsCanSend = false;

            sender.Stop();
            //if (!m_senders.Any(s => s.IsCanInitiate))
            //{
            //    Debug.WriteLine("MicroHelper PttStart m_waveIn.StopRecording()");
            //    //m_waveIn.StopRecording();
            //}
            //Debug.WriteLine("MicroHelper PttStop {0}, any senders has {1}", channel, m_senders.Any(s => s.IsCanInitiate));
        }
    }
}