﻿using VoiceStation.AppCreator.Creators;
using VoiceStation.AudioHelper.Abstractions;
using VoiceStation.AudioHelper.Concrete;
using VoiceStation.Multiton.Models;

namespace VoiceStation.AppCreator.Builders
{
    public class ListenHelperBuilder
    {
        private CodecCreator m_codecCreator;
        private PackageHandlerBuilder m_packageHandlerBuilder;
        private PlayerBuilder m_playerBuilder;
        private LogarithmConvertorCreator m_logarithmConvertorCreator;

        #region Builder Methods

        public ListenHelperBuilder WithCodecCreator(CodecCreator codecCreator)
        {
            m_codecCreator = codecCreator;
            return this;
        }

        public ListenHelperBuilder WithPackageHandlerBuilder(PackageHandlerBuilder packageHandlerBuilder)
        {
            m_packageHandlerBuilder = packageHandlerBuilder;
            return this;
        }

        public ListenHelperBuilder WithPlayerBuilder(PlayerBuilder playerBuilder)
        {
            m_playerBuilder = playerBuilder;
            return this;
        }

        public ListenHelperBuilder WithLogarithmConvertorCreator(LogarithmConvertorCreator logarithmConvertorCreator)
        {
            m_logarithmConvertorCreator = logarithmConvertorCreator;
            return this;
        }

        #endregion Builder Methods

        public IListenHelper Build(ChannelNumber channel)
        {
            return new ListenHelper(
                m_codecCreator.Create(),
                m_packageHandlerBuilder.Build(channel),
                m_playerBuilder.Build(),
                m_logarithmConvertorCreator.Create());
        }
    }
}