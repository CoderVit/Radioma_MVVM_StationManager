﻿using System;
using System.Linq;
using VoiceStation.AppHelper.Abstract;

namespace VoiceStation.AppHelper.Concrete
{
    /// <summary>
    /// Класс для проверки запущенных экземпляров приложений.
    /// В случае запуска, окно приложения выводится на передний план.
    /// </summary>
    public class AppChecker : IAppChecker
    {
        private readonly IProcessAcessor m_processHelper;
        private readonly IDllImportHelper m_dllImportHelper;

        public AppChecker(IProcessAcessor processHelper, IDllImportHelper dllImportHelper)
        {
            if (processHelper == null) throw new ArgumentNullException("processHelper");
            m_processHelper = processHelper;
            if (dllImportHelper == null) throw new ArgumentNullException("dllImportHelper");
            m_dllImportHelper = dllImportHelper;
        }

        /// <summary>
        /// проверка приложения на повторный запуск
        /// </summary>
        /// <returns>запущен процесс или нет</returns>
        public bool IsMainAppRunning()
        {
            var currentProcess = m_processHelper.CurrentProcess;

            var runningProcess = m_processHelper
                .Processes
                .FirstOrDefault(process => process.Id != currentProcess.Id && process.ProcessName.Equals(currentProcess.ProcessName, StringComparison.Ordinal));

            if (runningProcess == null) return false;
            m_dllImportHelper.ShowWindow(runningProcess.MainWindowHandle);
            return true;
        }
        /// <summary>
        /// Проверка запущен ли процесс
        /// </summary>
        /// <param name="name">Ключевое имя процесса</param>
        /// <returns>запущен процесс или нет</returns>
        public bool IsAppRunning(string name)
        {
            var process = m_processHelper.GetProcessByName(name);
            if (process == null) return false;
            m_dllImportHelper.ShowWindow(process.MainWindowHandle);
            return true;
        }
    }
}