﻿namespace VoiceStation.Engineer.SinHelper.Abstract
{
    public interface ISinStorage
    {
        short GetPoint();
    }
}
