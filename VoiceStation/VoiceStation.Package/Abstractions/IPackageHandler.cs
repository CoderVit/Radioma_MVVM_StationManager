﻿namespace VoiceStation.Package.Abstractions
{
    public interface IPackageHandler
    {
        void CheckSequence(byte smallByte, byte highByte);
        bool IsNative(byte smallByte, byte highByte);
    }
}