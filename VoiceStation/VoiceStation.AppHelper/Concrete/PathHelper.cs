﻿using System;
using System.IO;
using VoiceStation.AppHelper.Abstract;

namespace VoiceStation.AppHelper.Concrete
{
    public class PathHelper : IPathHelper
    {
        private readonly string m_path;

        public PathHelper(string path)
        {
            if (path == null) throw new ArgumentNullException("path");
            m_path = path;
        }

        public string PureFileName { get { return Path.GetFileNameWithoutExtension(m_path); } }

        public string FullPath { get { return Path.GetFullPath(m_path); } }

        public bool IsFileExist { get { return File.Exists(m_path); } }
    }
}
