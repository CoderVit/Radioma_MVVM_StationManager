﻿namespace VoiceStation.Codec.Models
{
    public struct Linear
    {
        public byte[] Waves { get; set; }
        public ushort Dispersion { get; set; }
    }
}