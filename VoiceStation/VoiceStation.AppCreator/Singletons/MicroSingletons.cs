﻿using VoiceStation.AppCreator.Creators;
using VoiceStation.Micro.Abstractions;
using VoiceStation.Micro.Concrete;

namespace VoiceStation.AppCreator.Singletons
{
    public class MicroHelperSingleton
    {
        private CodecCreator m_codecCreator;
        private WaveInCreator m_waveInCreator;
        private WaveConvertorCreater m_waveConvertorCreater;

        private IMicroHelper m_microHelper;

        #region Builder Methods

        public MicroHelperSingleton WithWaveInCreator(WaveInCreator creator)
        {
            m_waveInCreator = creator;
            return this;
        }

        public MicroHelperSingleton WithCodecCreator(CodecCreator codecCreator)
        {
            m_codecCreator = codecCreator;
            return this;
        }

        public MicroHelperSingleton WithWaveConvertorCreater(WaveConvertorCreater waveConvertorCreater)
        {
            m_waveConvertorCreater = waveConvertorCreater;
            return this;
        }

        #endregion Builder Methods

        public IMicroHelper Instance
        {
            get
            {
                return m_microHelper ?? (m_microHelper = new MicroHelper(
                    m_waveInCreator.CreateWaveIn(),
                    m_codecCreator.Create(),
                    m_waveConvertorCreater.Create()));
            }
        }
    }
}