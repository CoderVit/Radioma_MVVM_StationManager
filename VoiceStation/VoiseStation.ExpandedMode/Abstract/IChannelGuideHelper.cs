﻿using System;
using System.Windows.Threading;

namespace VoiceStation.ExpandedMode.Abstract
{
    public interface IChannelGuideHelper
    {
        event Action<decimal> OnFrequenceUpdate;
        bool IsConnected { get; }

        void ChannelGuideStart(Dispatcher dispatcher);
        void Reconnect();
    }
}