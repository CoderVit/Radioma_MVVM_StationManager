﻿using System;
using System.Windows.Input;
using VoiceStation.Handler.Abstract;
using VoiceStation.Handler.Models;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models
{
    public class CommonPttViewModel : ViewModel
    {
        private readonly IPttHandler m_pttHandler;

        public CommonPttViewModel(IPttHandler pttHandler)
        {
            if (pttHandler == null) throw new ArgumentNullException("pttHandler");
            m_pttHandler = pttHandler;
        }

        #region Commands

        public ICommand MouseDownCommand { get { return new StationCommand(() => m_pttHandler.MouseOn(PttStatement.PTT_STATEMENT_COMMON)); } }

        #endregion Commands

        #region Properties

        private bool m_isCommonPttPressed;
        public bool IsCommonPttPressed
        {
            get
            {
                return m_isCommonPttPressed;
            }
            set
            {
                m_isCommonPttPressed = value;
                RaisePropertyChanged("IsCommonPttPressed");
            }
        }

        #endregion Properties
    }
}