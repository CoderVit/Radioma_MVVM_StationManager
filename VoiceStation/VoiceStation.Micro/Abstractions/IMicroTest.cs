﻿using VoiceStation.Multiton.Models;

namespace VoiceStation.Micro.Abstractions
{
    public interface IMicroTest
    {
        void Add(ChannelNumber channelType);
        void Remove(ChannelNumber channelType);
        void Release();
    }
}