﻿using VoiceStation.Handler.Models;

namespace VoiceStation.Handler.Abstract
{
    public interface IPttHandler
    {
        void KeyBoardOn(PttStatement key);
        void KeyBoardOff(PttStatement key);
        void MouseOn(PttStatement key);
        void MouseOff();
    }
}