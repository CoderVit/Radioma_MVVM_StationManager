﻿using System.Windows;

namespace VoiceStation.UI.Views.Stations
{
    public partial class Channel
    {
        public Channel()
        {
            InitializeComponent();
        }

        #region Properties

        public string ChannelName { get; internal set; }

        private static readonly DependencyProperty srm_UserStationNameProperty = DependencyProperty.Register("UserStationName", typeof(string), typeof(Channel));

        public string UserStationName { get; set; }

        #endregion Properties
    }
}