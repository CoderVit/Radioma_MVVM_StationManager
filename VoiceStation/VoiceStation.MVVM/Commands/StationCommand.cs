﻿using System;
using System.Windows.Input;

namespace VoiceStation.MVVM.Commands
{
    public class StationCommand : ICommand
    {
        private readonly Action m_action;
        private readonly Action<object> m_parametrizedAction;

        private readonly bool m_canExecute;

        public event EventHandler CanExecuteChanged;

        public StationCommand(Action action, bool canExecute = true)
        {
            m_action = action;
            m_canExecute = canExecute;
        }

        public StationCommand(Action<object> action, bool canExecute = true)
        {
            m_parametrizedAction = action;
            m_canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return m_canExecute;
        }

        public void Execute(object parameter)
        {
            if (m_action != null)
            {
                m_action();
            }
            else if (m_parametrizedAction != null)
            {
                m_parametrizedAction(parameter);
            }
        }
    }
}
