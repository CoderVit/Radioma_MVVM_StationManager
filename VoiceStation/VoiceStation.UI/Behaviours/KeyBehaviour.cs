﻿using System.Windows;
using System.Windows.Input;

namespace VoiceStation.UI.Behaviours
{
    internal class KeyBehaviour
    {
        #region Key Up

        private static readonly DependencyProperty srm_KeyUpCommandProperty = DependencyProperty.RegisterAttached(
           "KeyUpCommand",
           typeof(ICommand),
           typeof(KeyBehaviour),
           new FrameworkPropertyMetadata(
               (o, args) => ((FrameworkElement)o).PreviewKeyUp +=
                   (sender, e) => ((ICommand)((FrameworkElement)sender).GetValue(srm_KeyUpCommandProperty)).Execute(e)));

        public static void SetKeyUpCommand(UIElement element, ICommand value)
        {
            element.SetValue(srm_KeyUpCommandProperty, value);
        }

        #endregion Key Up
    }
}