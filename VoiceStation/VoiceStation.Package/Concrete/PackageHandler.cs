﻿using System;
using VoiceStation.Package.Abstractions;

namespace VoiceStation.Package.Concrete
{
    public class PackageHandler : IPackageHandler
    {
        private readonly Action m_lostPackageNotifier;
        private readonly ushort m_chanelId;

        private ulong m_currentPackageNumber;

        public PackageHandler(Action lostPackageNotifier, ushort chanelId)
        {
            if (lostPackageNotifier == null) throw new ArgumentNullException("lostPackageNotifier");
            m_lostPackageNotifier = lostPackageNotifier;
            if (chanelId == 0) throw new ArgumentException("chanelId");
            m_chanelId = chanelId;
        }

        public void CheckSequence(byte smallByte, byte highByte)
        {
            var packageNumber = ToDecimal(smallByte, highByte);
            if (packageNumber <= ++m_currentPackageNumber) return;
            m_currentPackageNumber = packageNumber;
            m_lostPackageNotifier();
        }

        public bool IsNative(byte smallByte, byte highByte)
        {
            return ToDecimal(smallByte, highByte) == m_chanelId;
        }

        private ulong ToDecimal(byte smallByte, byte highByte)
        {
            return Convert.ToUInt64(smallByte << 8 | highByte);
        }
    }
}