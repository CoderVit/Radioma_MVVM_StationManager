﻿using System;
using VoiceStation.Common.Abstract;

namespace VoiceStation.Common.Concrete
{
    public class TimeAdapter : ITime
    {
        public DateTime Now { get { return DateTime.Now; } }
        public DateTime Default { get { return DateTime.MinValue; } }
    }
}