﻿using System;
using VoiceStation.AppCreator.Builders.UdpClients;
using VoiceStation.AppCreator.Singletons;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;
using VoiceStation.ExpandedMode.Abstract;
using VoiceStation.Handler.Abstract;
using VoiceStation.Handler.Models;
using VoiceStation.Multiton.Models;
using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Builders.ViewModels
{
    /// <summary>
    /// Строитель канальных моделей представления
    /// Строит модель, её зависимости и подписка событий
    /// </summary>
    public class ChannelsVmBuilder
    {
        private CommonClientBuilder m_commonClientBuilder;
        private ListenHelperBuilder m_listenHelperBuilder;
        private MicroHelperSingleton m_microHelper;
        private WaveSenderBuilder m_waveSenderBuilder;
        private IPttHandler m_pttHandler;
        private IMediator m_mediator;
        private ChannelGuideHelperBuilder m_channelGuideHelperBuilder;
        private IRrcHelper m_rrcHelper;

        #region Builder Methods

        public ChannelsVmBuilder WithCommonClientBuilder(CommonClientBuilder commonClientBuilder)
        {
            m_commonClientBuilder = commonClientBuilder;
            return this;
        }

        public ChannelsVmBuilder WithListenHelperBuilder(ListenHelperBuilder listenHelperBuilder)
        {
            m_listenHelperBuilder = listenHelperBuilder;
            return this;
        }

        public ChannelsVmBuilder WithMicroHelper(MicroHelperSingleton microHelper)
        {
            m_microHelper = microHelper;
            return this;
        }

        public ChannelsVmBuilder WithWaveSenderBuilder(WaveSenderBuilder waveSenderBuilder)
        {
            m_waveSenderBuilder = waveSenderBuilder;
            return this;
        }

        public ChannelsVmBuilder WithPttHandler(IPttHandler pttHandler)
        {
            m_pttHandler = pttHandler;
            return this;
        }

        public ChannelsVmBuilder WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public ChannelsVmBuilder WithChannelGuideHelperBuilder(ChannelGuideHelperBuilder builder)
        {
            m_channelGuideHelperBuilder = builder;
            return this;
        }

        public ChannelsVmBuilder WithRrcHelper(IRrcHelper rrcHelper)
        {
            m_rrcHelper = rrcHelper;
            return this;
        }

        #endregion Builder Methods

        public ChannelViewModel Build(ChannelNumber channel)
        {
            //Стоится первым, чтобы подписаться на событие приёма.
            var commonClient = m_commonClientBuilder.Build(channel);

            var waveSender = m_waveSenderBuilder.Build(channel);
            m_microHelper.Instance[(byte)channel] = waveSender;

            commonClient.OnPttStatusUpdate += b =>
            {
                if (waveSender.IsCanInitiate) waveSender.IsCanSend = b;
            };

            var listenHelper = m_listenHelperBuilder.Build(channel);

            var vm = new ChannelViewModel(
                commonClient,
                listenHelper,
                m_microHelper.Instance,
                channel);

            vm.OnMousePressed += () => m_pttHandler.MouseOn(ToPttStatement(channel));

            commonClient.OnStatusUpdate += status =>
            {
                if (!status && vm.IsPttPressed) vm.PttStop();
                vm.IsStationConnected = status;
            };
            commonClient.OnAudioReceived += bytes => listenHelper.HandleAudio(bytes);
            commonClient.PingStart();
            commonClient.RxStart();

            //подписка события listenHelper и передача логики в модель представления
            listenHelper.OnUpdateListenStatus += status => vm.ConnectionStatus = status;
            listenHelper.OnOutValueUpdated += value => vm.OutLevel = value;
            listenHelper.ListenStart();

            //регистрация сработанного события нажатия/отжатия манипуляторов
            m_mediator.RegisterStartPtt(ToStationMessage(channel, true), vm.PttStart);
            m_mediator.RegisterStopPtt(ToStationMessage(channel, false), vm.PttStop);

            //создание и связывание обертки RRCLib для получения и отоюражения частоты канала
            var channelGuideHelper = m_channelGuideHelperBuilder.Build(channel);
            channelGuideHelper.OnFrequenceUpdate += s => vm.Frequency = s;
            m_rrcHelper[(byte)channel] = channelGuideHelper;

            return vm;
        }
        /// <summary>
        /// Преобразование enum(канала) к enum(ptt состояния).
        /// </summary>
        /// <param name="channel">Номер канала</param>
        /// <returns></returns>
        private PttStatement ToPttStatement(ChannelNumber channel)
        {
            switch (channel)
            {
                case ChannelNumber.CHANNEL_NUMBER_FIRST:
                    return PttStatement.PTT_STATEMENT_FIRST;
                case ChannelNumber.CHANNEL_NUMBER_SECOND:
                    return PttStatement.PTT_STATEMENT_SECOND;
                case ChannelNumber.CHANNEL_NUMBER_THIRD:
                    return PttStatement.PTT_STATEMENT_THIRD;
                case ChannelNumber.CHANNEL_NUMBER_FOURTH:
                    return PttStatement.PTT_STATEMENT_FOURTH;
                default:
                    throw new ArgumentOutOfRangeException("channel", channel, null);
            }
        }
        /// <summary>
        /// Преобразование enum к enum.
        /// </summary>
        /// <param name="channel">Номер канала</param>
        /// <param name="isPressed">Флаг начала/конца ptt трансляции</param>
        /// <returns></returns>
        private StationMessage ToStationMessage(ChannelNumber channel, bool isPressed)
        {
            switch (channel)
            {
                case ChannelNumber.CHANNEL_NUMBER_FIRST:
                    return isPressed ? StationMessage.STATION_MESSAGE_FIRST_PTT_START : StationMessage.STATION_MESSAGE_FIRST_PTT_STOP;
                case ChannelNumber.CHANNEL_NUMBER_SECOND:
                    return isPressed ? StationMessage.STATION_MESSAGE_SECOND_PTT_START : StationMessage.STATION_MESSAGE_SECOND_PTT_STOP;
                case ChannelNumber.CHANNEL_NUMBER_THIRD:
                    return isPressed ? StationMessage.STATION_MESSAGE_THIRD_PTT_START : StationMessage.STATION_MESSAGE_THIRD_PTT_STOP;
                case ChannelNumber.CHANNEL_NUMBER_FOURTH:
                    return isPressed ? StationMessage.STATION_MESSAGE_FOURTH_PTT_START : StationMessage.STATION_MESSAGE_FOURTH_PTT_STOP;
                default:
                    throw new ArgumentOutOfRangeException("channel", channel, null);
            }
        }
    }
}