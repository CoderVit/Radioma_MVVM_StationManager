﻿namespace VoiceStation.AppHelper.Abstract
{
    public interface IAppManager
    {
        void ShowError(string message);
        void ShutDown();
    }
}
