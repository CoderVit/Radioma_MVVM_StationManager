﻿namespace VoiceStation.NetworkClients.Abstractions
{
    public interface IWaveSender
    {
        bool IsCanInitiate { get; set; }
        bool IsCanSend { get; set; }
        void Send(byte[] data);
        void Stop();
    }
}