﻿using System.Net;
using VoiceStation.Multiton.Models;

namespace VoiceStation.AppCreator.Builders.EndPoint
{
    public class LocalEndPointBuilder
    {
        private ushort[] m_localPorts;

        #region Builder Methods

        public LocalEndPointBuilder WithLocalPorts(ushort[] ports)
        {
            m_localPorts = ports;
            return this;
        }

        #endregion Builder Methods

        public IPEndPoint Build(ChannelNumber channel)
        {
            return new IPEndPoint(IPAddress.Any, m_localPorts[(byte)channel]);
        }
    }
}
