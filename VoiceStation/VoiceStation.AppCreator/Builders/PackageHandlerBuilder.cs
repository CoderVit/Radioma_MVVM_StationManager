﻿using System;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;
using VoiceStation.Multiton.Models;
using VoiceStation.Package.Abstractions;
using VoiceStation.Package.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class PackageHandlerBuilder
    {
        private IMediator m_mediator;
        private ushort[] m_channelIds;

        #region Builder Methods

        public PackageHandlerBuilder WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public PackageHandlerBuilder WithChannelIds(ushort[] channelIds)
        {
            m_channelIds = channelIds;
            return this;
        }

        #endregion Builder Methods

        public IPackageHandler Build(ChannelNumber channel)
        {
            return new PackageHandler(() => m_mediator.Notify(ToMessage(channel)), m_channelIds[(int)channel]);
        }

        //TODO reorganize it
        private StationMessage ToMessage(ChannelNumber channel)
        {
            switch (channel)
            {
                case ChannelNumber.CHANNEL_NUMBER_FIRST:
                    return StationMessage.STATION_MESSAGE_FIRST_LOST_PACKAGE;
                case ChannelNumber.CHANNEL_NUMBER_SECOND:
                    return StationMessage.STATION_MESSAGE_SECOND_LOST_PACKAGE;
                case ChannelNumber.CHANNEL_NUMBER_THIRD:
                    return StationMessage.STATION_MESSAGE_THIRD_LOST_PACKAGE;
                case ChannelNumber.CHANNEL_NUMBER_FOURTH:
                    return StationMessage.STATION_MESSAGE_FOURTH_LOST_PACKAGE;
                default:
                    throw new ArgumentOutOfRangeException("channel", channel, null);
            }
        }
    }
}