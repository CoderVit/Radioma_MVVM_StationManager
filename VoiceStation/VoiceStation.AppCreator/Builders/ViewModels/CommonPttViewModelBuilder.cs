﻿using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;
using VoiceStation.Handler.Abstract;
using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Builders.ViewModels
{
    public class CommonPttViewModelBuilder
    {
        private IMediator m_mediator;
        private IPttHandler m_handler;

        #region Builder Methods

        public CommonPttViewModelBuilder WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public CommonPttViewModelBuilder WithPttHandler(IPttHandler handler)
        {
            m_handler = handler;
            return this;
        }

        #endregion Builder Methods

        public CommonPttViewModel Build()
        {
            var vm = new CommonPttViewModel(m_handler);
            m_mediator.Register(StationMessage.STATION_MESSAGE_COMMON_PTT_START, () => { vm.IsCommonPttPressed = true; });
            m_mediator.Register(StationMessage.STATION_MESSAGE_COMMON_PTT_STOP, () => { vm.IsCommonPttPressed = false; });
            return vm;
        }
    }
}