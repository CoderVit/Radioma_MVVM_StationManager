﻿namespace VoiceStation.Handler.Models
{
    public enum PttInitiator
    {
        PTT_INITIATOR_OFF,
        PTT_INITIATOR_MOUSE,
        PTT_INITIATOR_KEYBOARD
    }
}