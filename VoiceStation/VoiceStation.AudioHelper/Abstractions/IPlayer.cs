﻿namespace VoiceStation.AudioHelper.Abstractions
{
    public interface IPlayer
    {
        void Play();
        void Stop();
        void AddSamples(byte[] samples);
    }
}
