﻿using System.Linq;
using VoiceStation.Convertors.Abstract;

namespace VoiceStation.Convertors.Concrete
{
    public class DispersionBuffer : IDispersionBuffer
    {
        private const byte ARRAY_SIZE = 8;
        private readonly ushort[] m_buffer;
        private readonly byte m_shiftedValue;

        private byte m_arrayPointer;

        public DispersionBuffer()
        {
            m_buffer = new ushort[ARRAY_SIZE];
            m_shiftedValue = 3;
            m_arrayPointer = 0;
        }

        public void Push(ushort dispersion)
        {
            m_buffer[m_arrayPointer++] = dispersion;
            if (m_arrayPointer == ARRAY_SIZE) m_arrayPointer = 0;
        }

        public ushort GetMiddle()
        {
            return (ushort)(m_buffer.Sum(s => s) >> m_shiftedValue);
        }
    }
}