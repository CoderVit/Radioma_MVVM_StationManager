﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VoiceStation.AudioHelper.Abstractions;
using VoiceStation.Convertors.Abstract;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models
{
    public class VolumeViewModel : ViewModel
    {
        private readonly IVolumeManager m_volumeManager;
        private readonly IVolumeConvertor m_volumeConvertor;

        private const byte THREAD_SLEEP_TIME = 100;

        public VolumeViewModel(IVolumeManager volumeManager, IVolumeConvertor volumeConvertor)
        {
            if (volumeManager == null) throw new ArgumentNullException("volumeManager");
            m_volumeManager = volumeManager;
            if (volumeConvertor == null) throw new ArgumentNullException("volumeConvertor");
            m_volumeConvertor = volumeConvertor;
        }

        public VolumeViewModel ViewModelStart()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Thread.Sleep(THREAD_SLEEP_TIME);
                    IsMute = m_volumeManager.IsMute;
                    VolumeLevel = m_volumeManager.SystemVolume;
                }
            });

            return this;
        }

        #region Properties

        private float m_volumeLevel;
        public float VolumeLevel
        {
            get { return m_volumeLevel; }
            set
            {
                if (Math.Abs(m_volumeLevel - value) <= 0) return;
                m_volumeManager.SystemVolume = value;
                m_volumeLevel = value;
                RaisePropertyChanged("VolumeLevel");
                RaisePropertyChanged("VolumeLevelFormat");
            }
        }

        private bool m_isMute;
        public bool IsMute
        {
            get { return m_isMute; }
            set
            {
                if (m_isMute == value) return;
                m_isMute = value;
                RaisePropertyChanged("IsMute");
            }
        }

        //TODO get anoter way to avoid this property
        public byte VolumeLevelFormat { get { return m_volumeConvertor.FromSystemVolume(VolumeLevel); } }

        #endregion Properties

        #region Commands

        public ICommand MuteToggleCommand { get { return new StationCommand(isChecked => m_volumeManager.IsMute = (bool)isChecked); } }

        public ICommand IncreaseVolumeCommand { get { return new StationCommand(() => VolumeLevel = VolumeLevel + (float)0.01 < 1 ? VolumeLevel + (float)0.01 : 1); } }

        public ICommand DecreaseVolumeCommand { get { return new StationCommand(() => VolumeLevel = VolumeLevel - (float)0.01 > 0 ? VolumeLevel - (float)0.01 : 0); } }

        #endregion Commands
    }
}