﻿using System.Text;
using VoiceStation.SettingHelper.Abstract;
using VoiceStation.SettingHelper.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class EncryptManagerBuilder
    {
        private byte[] m_saltKeyBytes;

        #region Builder Methods

        public EncryptManagerBuilder WithSaltKey(string key)
        {
            m_saltKeyBytes = Encoding.UTF8.GetBytes(key);
            return this;
        }

        #endregion Builder Methods

        public IEncryptManager Build()
        {
            return new EncryptManager(m_saltKeyBytes);
        }
    }
}