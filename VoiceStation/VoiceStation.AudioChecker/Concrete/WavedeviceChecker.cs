﻿using NAudio.Wave;
using System;
using VoiceStation.AudioChecker.Abstract;

namespace VoiceStation.AudioChecker.Concrete
{
    public class WaveDeviceChecker : IWaveDeviceChecker
    {
        private readonly string m_defaultDeviceName;

        public byte WaveInNumber
        {
            get
            {
                if (WaveIn.DeviceCount == 0) throw new ApplicationException("Отсутствует микрофон");
                for (byte deviceId = 0; deviceId < WaveIn.DeviceCount; deviceId++)
                {
                    if (WaveIn.GetCapabilities(deviceId).ProductName.Contains(m_defaultDeviceName))
                        return deviceId;
                }
                return 0;
            }
        }

        public byte WaveOutNumber
        {
            get
            {
                if (WaveOut.DeviceCount == 0) throw new ApplicationException("Отсутствует устройство вывода звука");
                for (byte deviceId = 0; deviceId < WaveOut.DeviceCount; deviceId++)
                {
                    if (WaveOut.GetCapabilities(deviceId).ProductName.Contains(m_defaultDeviceName))
                        return deviceId;
                }
                return 0;
            }
        }

        public WaveDeviceChecker(string defaultDeviceName)
        {
            m_defaultDeviceName = defaultDeviceName;
        }
    }
}