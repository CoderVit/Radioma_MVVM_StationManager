﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models.Windows
{
    public class UserSettingViewModel : ViewModel, IModalViewModel
    {
        public event Action<Collection<string>> OnNamesChanged;
        public event Action OnWindowClose;

        private void SaveNames()
        {
            OnNamesChanged(EditingNames);
            OnWindowClose();
        }

        #region Properties

        private ushort m_microLevel;
        public ushort MicroLevel
        {
            get
            {
                return m_microLevel;

            }
            set
            {
                m_microLevel = value;
                RaisePropertyChanged("MicroLevel");
            }
        }

        public ObservableCollection<string> EditingNames { get; set; }

        #endregion Properties

        #region Commands

        public ICommand SaveCommand { get { return new StationCommand(SaveNames); } }

        public ICommand CloseCommand { get { return new StationCommand(OnWindowClose); } }

        #endregion Commands
    }
}