﻿namespace VoiceStation.Engineer.SinHelper.Abstract
{
    public interface ISinWaveHelper
    {
        short[] GetPackage();
    }
}