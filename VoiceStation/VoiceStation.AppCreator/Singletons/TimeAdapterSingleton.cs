﻿using VoiceStation.Common.Abstract;
using VoiceStation.Common.Concrete;

namespace VoiceStation.AppCreator.Singletons
{
    public class TimeAdapterSingleton
    {
        private ITime m_time;

        public ITime Instanse { get { return m_time ?? (m_time = new TimeAdapter()); } }
    }
}