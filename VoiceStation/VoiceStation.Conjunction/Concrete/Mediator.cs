﻿using System;
using System.Windows.Threading;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;

namespace VoiceStation.Conjunction.Concrete
{
    public class Mediator : DispatcherObject, IMediator
    {
        private readonly IMediatorMap m_mediatorStorage;

        public Mediator(IMediatorMap mediatorStorage)
        {
            if (mediatorStorage == null) throw new ArgumentNullException("mediatorStorage");
            m_mediatorStorage = mediatorStorage;
        }

        public void Register(StationMessage message, Action callback)
        {
            m_mediatorStorage.AddAction(message, callback);
        }

        public void RegisterStartPtt(StationMessage message, Action callback)
        {
            m_mediatorStorage.AddAction(message, callback);
            m_mediatorStorage.AddAction(StationMessage.STATION_MESSAGE_COMMON_PTT_START, callback);
        }

        public void RegisterStopPtt(StationMessage message, Action callback)
        {
            m_mediatorStorage.AddAction(message, callback);
            m_mediatorStorage.AddAction(StationMessage.STATION_MESSAGE_COMMON_PTT_STOP, callback);
        }

        public void Notify(StationMessage message)
        {
            if (CheckAccess())
            {
                var actions = m_mediatorStorage.GetActions(message);

                if (actions != null) actions.ForEach(action => action());
            }
            else
            {
                Dispatcher.BeginInvoke((Action)delegate
                {
                    Notify(message);
                },
                DispatcherPriority.Send);
            }
        }
    }
}