﻿using System.Collections.ObjectModel;
using System.Windows;
using VoiceStation.AppCreator.Builders;
using VoiceStation.AppCreator.Builders.EndPoint;
using VoiceStation.AppCreator.Builders.Sin;
using VoiceStation.AppCreator.Builders.UdpClients;
using VoiceStation.AppCreator.Builders.ViewModels;
using VoiceStation.AppCreator.Creators;
using VoiceStation.AppCreator.Singletons;
using VoiceStation.AppCreator.Singletons.ViewModels;
using VoiceStation.AppHelper.Abstract;
using VoiceStation.AudioChecker.Abstract;
using VoiceStation.Config.Models;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.ExpandedMode.Abstract;
using VoiceStation.ExpandedMode.Concrete;
using VoiceStation.Handler.Abstract;
using VoiceStation.Multiton.Models;
using VoiceStation.MVVM.Models;
using VoiceStation.MVVM.Models.Base;
using VoiceStation.MVVM.Models.Windows;
using VoiceStation.SettingHelper.Abstract;
using VoiceStation.UI.Views.Windows.Base;
using VoiceStation.UI.Views.Windows.Modals;

namespace VoiceStation.UI.Models
{
    internal class ViewModels
    {
        private IMediator m_mediator;
        private StationConfig m_config;
        private IWaveDeviceChecker m_waveDeviceChecker;
        private INamesHelper m_namesHelper;
        private ChannelsVmBuilder m_channelsModelBuilder;
        private IPttHandler m_pttHandler;
        private UserSettingVmBuilder m_settingVmBuilder;
        private IAppManager m_appManager;
        private DebugVmSingleton m_debugVmSingleton;
        private ExpandedModeVmSingleton m_expandedModeVmSingleton;
        private CodecCreator m_codecCreator;
        private WaveFormatSingleton m_waveFormatSingleton;

        private IRrcHelper m_rrcHelper;

        private void Init()
        {
            var waveInCreator = new WaveInCreator().WithDeviceNumber(m_waveDeviceChecker.WaveInNumber).WithWaveFormatSingleton(m_waveFormatSingleton).WithBufferLength(20);

            var controlClientBuilder = new ControlClientBuilder()
                .WithUdpCreator(new UdpClientBuilder(m_config.ReceiveTimeOut))
                .WithLocalEndPointBuilder(new LocalEndPointBuilder().WithLocalPorts(new ushort[] { 60001, 60002, 60003, 60004 }))
                .WithRemoteEndPointCreator(new RemoteEndPointBuilder().WithAddresses(m_config.Addresses).AndPorts(m_config.DestinationPorts));

            var waveSenderBuilder = new WaveSenderBuilder().WithClientBuilder(controlClientBuilder).WithPackageCreatorBuilder(new PackageCreatorBuilder().AndHeader(128));
            m_rrcHelper = new RrcHelper();
            m_channelsModelBuilder = new ChannelsVmBuilder()
                    .WithCommonClientBuilder(new CommonClientBuilder().WithClientBuilder(controlClientBuilder))
                    .WithListenHelperBuilder(
                        new ListenHelperBuilder()
                            .WithCodecCreator(m_codecCreator)
                            .WithPackageHandlerBuilder(new PackageHandlerBuilder().WithMediator(m_mediator).WithChannelIds(m_config.ChanelIds))
                            .WithPlayerBuilder(new PlayerBuilder().WithWaveOutCreator(new WaveOutCreator().WithDeviceNumber(m_waveDeviceChecker.WaveOutNumber)).WithWaveProviderCreator(new BufferedWaveProviderBuilder().WithWaveFormat(m_waveFormatSingleton)))
                            .WithLogarithmConvertorCreator(new LogarithmConvertorCreator()))
                    .WithMicroHelper(new MicroHelperSingleton().WithWaveInCreator(waveInCreator).WithCodecCreator(m_codecCreator).WithWaveConvertorCreater(new WaveConvertorCreater()))
                    .WithWaveSenderBuilder(waveSenderBuilder)
                    .WithPttHandler(m_pttHandler)
                    .WithMediator(m_mediator)
                    .WithChannelGuideHelperBuilder(new ChannelGuideHelperBuilder().WithConnectionStrings(new[] { "F8101:UDP:192.168.0.211:60003:8A:E0", "A120:UDP:192.168.0.212:60003:92:E0", "F5061D:UDP:192.168.0.213:60003", "F6061D:UDP:192.168.0.214:60003" }))
                    .WithRrcHelper(m_rrcHelper);

            m_debugVmSingleton = new DebugVmSingleton().WithMediator(m_mediator).WithSinHelperBuilder(
                    new SinHelperBuilder()
                        .WithWaveInCreator(waveInCreator)
                        .WithCodecCreator(m_codecCreator)
                        .WithSinWaveHelperBuilder(new SinWaveHelperBuilder().WithSinStorageBuilder(new SinStorageBuilder()))
                        .WithWaveSenderPoolBuilder(new WaveSenderPoolBuilder().WithWaveSenderBuilder(waveSenderBuilder)));

            m_settingVmBuilder = new UserSettingVmBuilder()
                .WithWaveInCreator(waveInCreator)
                .WithDispersionHelperBuilder(new DispersionHelperBuilder().WithDispersionBufferBuilder(new DispersionBufferCreator()));

            m_mainWindowViewModel = new MainWindowVmBuilder().WithMediator(m_mediator).WithPttHandler(m_pttHandler).WithNamesHelper(m_namesHelper).WithAppManager(m_appManager).Build();

            m_expandedModeVmSingleton = new ExpandedModeVmSingleton().WithRrcHelper(m_rrcHelper);
        }

        #region Builder Methods

        public ViewModels WithMediator(IMediator mediator)
        {
            m_mediator = mediator;
            return this;
        }

        public ViewModels WithConfig(StationConfig config)
        {
            m_config = config;
            return this;
        }

        public ViewModels WithNamesHelper(INamesHelper namesHelper)
        {
            m_namesHelper = namesHelper;
            return this;
        }

        public ViewModels WithPttHandler(IPttHandler pttHandler)
        {
            m_pttHandler = pttHandler;
            return this;
        }

        public ViewModels WithAppManager(IAppManager appManager)
        {
            m_appManager = appManager;
            return this;
        }

        public ViewModels WithCodecCreator(CodecCreator codecCreator)
        {
            m_codecCreator = codecCreator;
            return this;
        }

        public ViewModels WithWaveFormatSingleton(WaveFormatSingleton waveFormatSingleton)
        {
            m_waveFormatSingleton = waveFormatSingleton;
            return this;
        }

        public ViewModels WithDeviceChecker(IWaveDeviceChecker waveDeviceChecker)
        {
            m_waveDeviceChecker = waveDeviceChecker;
            Init();
            return this;
        }

        #endregion Builder Methods

        #region Windows View Models

        private MainWindowViewModel m_mainWindowViewModel;
        public MainWindowViewModel MainWindowViewModel { get { return m_mainWindowViewModel; } }

        public SettingsViewModel SettingsViewModel
        {
            get
            {
                //создание модели представления
                var settingControlVm = new SettingVmCreator().Create();
                //подписка на событие клика на настройках пользователя
                settingControlVm.onUserSettingButtonClick += () =>
                {
                    var vm = m_settingVmBuilder.Build();
                    vm.EditingNames = new ObservableCollection<string>(m_mainWindowViewModel.StationNames);
                    vm.OnNamesChanged += namesCollection =>
                    {
                        m_namesHelper.SetNames(namesCollection);
                        m_mainWindowViewModel.StationNames = namesCollection;
                    };

                    ShowWindow<UserSettingWindow>("Настройки", vm);
                };
                //подписка на событие клика на режиме отладки
                settingControlVm.onDebugButtonClick += () => ShowWindow<DebugWindow>("Отладка", m_debugVmSingleton.Instance);
                //подписка на событие смены режима настроек(пользовательский/инженерный)
                m_mainWindowViewModel.OnSettingModeChange += () => settingControlVm.IsEngineerMode = !settingControlVm.IsEngineerMode;

                return settingControlVm;
            }
        }

        #endregion Windows View Models

        #region Channels View Models

        public ChannelViewModel F8101ViewModel { get { return m_channelsModelBuilder.Build(ChannelNumber.CHANNEL_NUMBER_FIRST); } }

        public ChannelViewModel A120ViewModel { get { return m_channelsModelBuilder.Build(ChannelNumber.CHANNEL_NUMBER_SECOND); } }

        public ChannelViewModel F5061ViewModel { get { return m_channelsModelBuilder.Build(ChannelNumber.CHANNEL_NUMBER_THIRD); } }

        public ChannelViewModel F6061ViewModel { get { return m_channelsModelBuilder.Build(ChannelNumber.CHANNEL_NUMBER_FOURTH); } }

        #endregion Channels View Models

        public ExpandedModeViewModel ExpandedModeViewModel { get { return m_expandedModeVmSingleton.Instance; } }
        /// <summary>
        /// создание шаблонного окна и его диалоговое отображение
        /// </summary>
        /// <typeparam name="T">Базовый класс содержащий в себе простое модальное окно с простыми базовыми полями и коммандами</typeparam>
        /// <param name="header">Строка заголовка модального окна</param>
        /// <param name="modalViewModel">модель представления реализующая интерфейс IModalViewModel</param>
        private void ShowWindow<T>(string header, IModalViewModel modalViewModel) where T : ModalWindow, new()
        {
            var window = new T
            {
                Header = header,
                Owner = Application.Current.MainWindow,
                DataContext = modalViewModel
            };
            modalViewModel.OnWindowClose += () => window.CloseWindowCommand.Execute(null);
            window.ShowDialog();
        }
    }
}