﻿using NAudio.Wave;
using VoiceStation.AppCreator.Singletons;

namespace VoiceStation.AppCreator.Creators
{
    public class WaveInCreator
    {
        private byte m_number;
        private WaveFormatSingleton m_waveFormatSingleton;
        private byte m_bufferLength;

        #region Builder Methods

        public WaveInCreator WithDeviceNumber(byte number)
        {
            m_number = number;
            return this;
        }

        public WaveInCreator WithWaveFormatSingleton(WaveFormatSingleton waveFormatSingleton)
        {
            m_waveFormatSingleton = waveFormatSingleton;
            return this;
        }

        public WaveInCreator WithBufferLength(byte length)
        {
            m_bufferLength = length;
            return this;
        }

        #endregion Builder Methods

        public IWaveIn CreateWaveIn()
        {
            return new WaveIn
            {
                DeviceNumber = m_number,
                WaveFormat = m_waveFormatSingleton.Instance,
                BufferMilliseconds = m_bufferLength
            };
        }
    }
}