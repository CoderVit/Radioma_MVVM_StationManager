﻿using System.Windows;

namespace VoiceStation.UI.Views.Controls
{
    public partial class StationSlider
    {
        public double SliderHeight { get; set; }

        private static readonly DependencyProperty srm_SliderValueProperty = DependencyProperty.Register("SliderValue", typeof(double), typeof(StationSlider), new UIPropertyMetadata(null));
        public double SliderValue
        {
            get { return (double)GetValue(srm_SliderValueProperty); }
            set { SetValue(srm_SliderValueProperty, value); }
        }

        public StationSlider()
        {
            InitializeComponent();
        }
    }
}