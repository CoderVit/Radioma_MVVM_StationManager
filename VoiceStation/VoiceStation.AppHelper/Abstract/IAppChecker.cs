﻿namespace VoiceStation.AppHelper.Abstract
{
    public interface IAppChecker
    {
        bool IsMainAppRunning();
        bool IsAppRunning(string name);
    }
}