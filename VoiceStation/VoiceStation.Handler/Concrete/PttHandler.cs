﻿using System;
using System.Collections.Generic;
using System.Linq;
using VoiceStation.Common.Abstract;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Models;
using VoiceStation.Handler.Abstract;
using VoiceStation.Handler.Models;

namespace VoiceStation.Handler.Concrete
{
    public class PttHandler : IPttHandler
    {
        private readonly IMediator m_mediator;
        private readonly ITime m_time;

        private readonly Dictionary<PttStatement, PttInitiator> m_keyStatements;
        private const ushort DELAY_PERIOD = 300;

        private DateTime m_lastActivityTime;

        public PttHandler(IMediator mediator, ITime time)
            : this(time)
        {
            if (mediator == null) throw new ArgumentNullException("mediator");
            m_mediator = mediator;
            if (time == null) throw new ArgumentNullException("time");
            m_time = time;
        }

        private PttHandler(ITime time)
        {
            m_keyStatements = new Dictionary<PttStatement, PttInitiator>
            {
                { PttStatement.PTT_STATEMENT_COMMON, PttInitiator.PTT_INITIATOR_OFF },
                { PttStatement.PTT_STATEMENT_FIRST, PttInitiator.PTT_INITIATOR_OFF },
                { PttStatement.PTT_STATEMENT_SECOND, PttInitiator.PTT_INITIATOR_OFF },
                { PttStatement.PTT_STATEMENT_THIRD, PttInitiator.PTT_INITIATOR_OFF },
                { PttStatement.PTT_STATEMENT_FOURTH, PttInitiator.PTT_INITIATOR_OFF }
            };
            m_lastActivityTime = time.Default;
        }

        public void KeyBoardOn(PttStatement key)
        {
            //Debug.WriteLine("key pressed");
            if (!IsCanPressed()) return;
            //Debug.WriteLine("key pressed after validation");
            m_keyStatements[key] = PttInitiator.PTT_INITIATOR_KEYBOARD;
            InitiateOn(key);
        }

        public void KeyBoardOff(PttStatement key)
        {
            //Debug.WriteLine("key unpressed");
            var statement = m_keyStatements.FirstOrDefault(initiator => initiator.Value == PttInitiator.PTT_INITIATOR_KEYBOARD
                && initiator.Key == key);
            if (statement.Key == 0) return;
            //Debug.WriteLine("key unpressed after validation");
            m_keyStatements[statement.Key] = PttInitiator.PTT_INITIATOR_OFF;
            InitiateOff(statement.Key);
        }

        public void MouseOn(PttStatement key)
        {
            if (!IsCanPressed()) return;
            m_keyStatements[key] = PttInitiator.PTT_INITIATOR_MOUSE;
            InitiateOn(key);
        }

        public void MouseOff()
        {
            var statement = m_keyStatements.FirstOrDefault(initiator => initiator.Value == PttInitiator.PTT_INITIATOR_MOUSE);
            if (statement.Key == 0) return;
            m_keyStatements[statement.Key] = PttInitiator.PTT_INITIATOR_OFF;
            InitiateOff(statement.Key);
        }
        /// <summary>
        /// Проверка что не активно не одно состояние PTT и время после последнего отжатия манипулятора более чем DELAY_PERIOD(мс)
        /// </summary>
        /// <returns></returns>
        private bool IsCanPressed()
        {
            //var st1 = !m_keyStatements.Values.Any(init => init == PttInitiator.PTT_INITIATOR_MOUSE || init == PttInitiator.PTT_INITIATOR_KEYBOARD);

            //Debug.WriteLine(st1);
            //var st2 = m_time.Now.Subtract(m_lastActivityTime).TotalMilliseconds > DELAY_PERIOD;
            //Debug.WriteLine(st2);
            //var st3 = st1 && st2;
            //Debug.WriteLine(st3);
            //return st3;

            //var t = m_time.Now.Subtract(m_lastActivityTime).TotalMilliseconds;
            //Debug.WriteLine("{0} - {1} - {2}", t, t > DELAY_PERIOD, m_keyStatements.Values.Any(init => init == PttInitiator.PTT_INITIATOR_MOUSE || init == PttInitiator.PTT_INITIATOR_KEYBOARD));
            return !m_keyStatements.Values.Any(init => init == PttInitiator.PTT_INITIATOR_MOUSE || init == PttInitiator.PTT_INITIATOR_KEYBOARD)
                && m_time.Now.Subtract(m_lastActivityTime).TotalMilliseconds > DELAY_PERIOD;
        }

        private void InitiateOn(PttStatement key)
        {
            switch (key)
            {
                case PttStatement.PTT_STATEMENT_FIRST:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_FIRST_PTT_START);
                    break;
                case PttStatement.PTT_STATEMENT_SECOND:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_SECOND_PTT_START);
                    break;
                case PttStatement.PTT_STATEMENT_THIRD:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_THIRD_PTT_START);
                    break;
                case PttStatement.PTT_STATEMENT_FOURTH:

                    m_mediator.Notify(StationMessage.STATION_MESSAGE_FOURTH_PTT_START);
                    break;
                case PttStatement.PTT_STATEMENT_COMMON:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_COMMON_PTT_START);
                    break;
            }
        }

        private void InitiateOff(PttStatement key)
        {
            switch (key)
            {
                case PttStatement.PTT_STATEMENT_FIRST:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_FIRST_PTT_STOP);
                    break;
                case PttStatement.PTT_STATEMENT_SECOND:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_SECOND_PTT_STOP);
                    break;
                case PttStatement.PTT_STATEMENT_THIRD:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_THIRD_PTT_STOP);
                    break;
                case PttStatement.PTT_STATEMENT_FOURTH:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_FOURTH_PTT_STOP);
                    break;
                case PttStatement.PTT_STATEMENT_COMMON:
                    m_mediator.Notify(StationMessage.STATION_MESSAGE_COMMON_PTT_STOP);
                    break;
            }
            m_lastActivityTime = m_time.Now;
        }
    }
}