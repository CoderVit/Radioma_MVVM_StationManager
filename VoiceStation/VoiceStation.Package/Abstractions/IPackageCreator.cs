﻿namespace VoiceStation.Package.Abstractions
{
    public interface IPackageCreator
    {
        byte[] GetPackage(byte[] waveBytes);
        void ChangeRandom();
    }
}