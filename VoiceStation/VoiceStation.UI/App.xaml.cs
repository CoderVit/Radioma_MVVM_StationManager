﻿using System;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Threading;
using VoiceStation.AppCreator.Builders;
using VoiceStation.AppCreator.Builders.ViewModels;
using VoiceStation.AppCreator.Creators;
using VoiceStation.AppCreator.Singletons;
using VoiceStation.AppHelper.Concrete;
using VoiceStation.AudioChecker.Concrete;
using VoiceStation.Config.Models;
using VoiceStation.UI.Models;

namespace VoiceStation.UI
{
    public partial class App
    {
        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }

        [STAThread]
        public static void Main()
        {
            var appChecker = new AppCheckerBuilder().WithProcessAcessor(new ProcessAcessor()).WithDllImportHelper(new DllImportHelper()).Build();
            if (appChecker.IsMainAppRunning()) return;

            var app = new App();
            var appManager = new AppManager();

            try
            {
                var mediator = new MediatorBuilder().WithMediatorMapperBuilder(new MapperBuilder()).Build();
                var timeAdapter = new TimeAdapterSingleton();
                var pttHandler = new PttHandlerBuilder().WithMediator(mediator).WithTimeAdapter(timeAdapter.Instanse).Build();
                var config = new ConfigHelperBuilder().WithConfigPath("conf.json").WithSerializer(new JavaScriptSerializer()).WithConvertors(new StationConverter()).Build().Read();

                app.Resources.Add("Locator", new ViewModels()
                    .WithMediator(mediator)
                    .WithConfig(config)
                    .WithNamesHelper(new NamesHelperBuilder().WithEncryptManager(new EncryptManagerBuilder().WithSaltKey("22081984")).WithSettingPath("thumb").Build())
                    .WithPttHandler(pttHandler)
                    .WithAppManager(appManager)
                    .WithCodecCreator(new CodecCreator())
                    .WithWaveFormatSingleton(new WaveFormatSingleton())
                    .WithDeviceChecker(new WaveDeviceChecker("Plantronics")));

                app.Resources.Add("CommonPttVm", new CommonPttViewModelBuilder().WithMediator(mediator).WithPttHandler(pttHandler).Build());
                app.Resources.Add("VolumeVm", new VolumeVmBuilder().WithVolumeConvertorBuilder(new VolumeConvertorCreator()).WithVolumeManagerBuilder(new SystemVolumeManagerBuilder()).Build());
                app.Resources.Add("TimeVm", new TimeVmBuilder().WithTimeAdapter(timeAdapter.Instanse).Build());
                var pathHelper = new PathHelper(config.RrcPath);
                app.Resources.Add("InvokerVm", new InvokeRrcBuilder().WithProcessInvoker(new ProcessInvoker(appChecker, pathHelper)).WithPathHelper(pathHelper).Create());
            }
            catch (Exception exception)
            {
                appManager.ShowError(exception.Message);
                appManager.ShutDown();
            }

            app.InitializeComponent();
            app.Run();
        }
    }
}