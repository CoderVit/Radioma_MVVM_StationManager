﻿using System;
using System.IO;
using System.Web.Script.Serialization;
using VoiceStation.Config.Abstract;
using VoiceStation.Config.Models;

namespace VoiceStation.Config.Concrete
{
    public class ConfigHelper : IConfigHelper
    {
        private readonly string m_path;
        private readonly JavaScriptSerializer m_serializer;

        public ConfigHelper(string path, JavaScriptSerializer serializer)
        {
            if (path == null) throw new ArgumentNullException("path");
            m_path = path;
            if (serializer == null) throw new ArgumentNullException("serializer");
            m_serializer = serializer;
        }

        public StationConfig Read()
        {
            using (var streamReader = new StreamReader(m_path))
            {
                return m_serializer.Deserialize<StationConfig>(streamReader.ReadToEnd());
            }
        }
    }
}