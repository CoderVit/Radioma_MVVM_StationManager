﻿using System;
using System.Collections.Generic;
using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Concrete;
using VoiceStation.Conjunction.Models;

namespace VoiceStation.AppCreator.Builders
{
    public class MapperBuilder
    {
        private readonly Dictionary<StationMessage, List<Action>> m_map;

        public MapperBuilder()
        {
            m_map = new Dictionary<StationMessage, List<Action>>();
        }

        public IMediatorMap Build()
        {
            return new MediatorMapper(m_map);
        }
    }
}