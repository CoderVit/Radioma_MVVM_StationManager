﻿using System.Threading;
using System.Windows.Threading;
using VoiceStation.ExpandedMode.Abstract;

namespace VoiceStation.ExpandedMode.Concrete
{
    public class RrcHelper : IRrcHelper
    {
        private readonly IChannelGuideHelper[] m_channelGuideHelpers;
        private readonly Dispatcher m_dispatcher;

        private const byte ARRAY_SIZE = 4;
        private Timer m_timer;

        public RrcHelper()
        {
            m_channelGuideHelpers = new IChannelGuideHelper[ARRAY_SIZE];
            m_dispatcher = Dispatcher.CurrentDispatcher;
        }

        public IChannelGuideHelper this[byte index]
        {
            set { m_channelGuideHelpers[index] = value; }
        }

        public void TryToEnable()
        {
            for (byte channelIterator = 0; channelIterator < ARRAY_SIZE; channelIterator++)
            {
                m_channelGuideHelpers[channelIterator].ChannelGuideStart(m_dispatcher);
            }
        }

        public void RealTimeWorkStart()
        {
            m_timer = new Timer(state =>
            {
                for (byte channelIterator = 0; channelIterator < ARRAY_SIZE; channelIterator++)
                {
                    var channelGuide = m_channelGuideHelpers[channelIterator];
                    if (!channelGuide.IsConnected) channelGuide.Reconnect();
                }
            }, null, 1500, 1000);
        }
    }
}