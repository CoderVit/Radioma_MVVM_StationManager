﻿using VoiceStation.Codec.Abstraction;
using VoiceStation.Codec.Concrete;

namespace VoiceStation.AppCreator.Creators
{
    public class CodecCreator
    {
        public IG711 Create()
        {
            return new G711();
        }
    }
}