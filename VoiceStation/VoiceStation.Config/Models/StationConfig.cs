﻿using System.Net;

namespace VoiceStation.Config.Models
{
    public class StationConfig
    {
        public string RrcPath { get; set; }
        public ushort ReceiveTimeOut { get; set; }
        public ushort[] DestinationPorts { get; set; }
        public IPAddress[] Addresses { get; set; }
        public ushort[] ChanelIds { get; set; }
    }
}