﻿using VoiceStation.AppCreator.Creators;
using VoiceStation.AudioHelper.Abstractions;
using VoiceStation.AudioHelper.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class PlayerBuilder
    {
        private WaveOutCreator m_waveOutCreator;
        private BufferedWaveProviderBuilder m_waveProviderCreator;

        #region Builder Methods

        public PlayerBuilder WithWaveOutCreator(WaveOutCreator waveOutCreator)
        {
            m_waveOutCreator = waveOutCreator;
            return this;
        }

        public PlayerBuilder WithWaveProviderCreator(BufferedWaveProviderBuilder waveProviderCreator)
        {
            m_waveProviderCreator = waveProviderCreator;
            return this;
        }

        #endregion Builder Methods

        public IPlayer Build()
        {
            var waveOut = m_waveOutCreator.Create();
            var waveProvider = m_waveProviderCreator.Build();
            waveOut.Init(waveProvider);
            return new Player(waveOut, waveProvider);
        }
    }
}