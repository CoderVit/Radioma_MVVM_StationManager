﻿using NAudio.Wave;
using VoiceStation.AppCreator.Singletons;

namespace VoiceStation.AppCreator.Builders
{
    public class BufferedWaveProviderBuilder
    {
        private WaveFormatSingleton m_waveFormatSingleton;

        #region Builder Methods

        public BufferedWaveProviderBuilder WithWaveFormat(WaveFormatSingleton waveFormatSingleton)
        {
            m_waveFormatSingleton = waveFormatSingleton;
            return this;
        }

        #endregion Builder Methods

        public BufferedWaveProvider Build()
        {
            return new BufferedWaveProvider(m_waveFormatSingleton.Instance);
        }
    }
}