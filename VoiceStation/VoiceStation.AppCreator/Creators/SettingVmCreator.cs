﻿using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Creators
{
    public class SettingVmCreator
    {
        public SettingsViewModel Create()
        {
            return new SettingsViewModel();
        }
    }
}