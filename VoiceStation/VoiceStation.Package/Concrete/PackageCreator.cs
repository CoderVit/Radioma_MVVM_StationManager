﻿using System;
using VoiceStation.Package.Abstractions;

namespace VoiceStation.Package.Concrete
{
    public class PackageCreator : IPackageCreator
    {
        private readonly byte[] m_header;

        private ushort m_packageNumber;
        private int m_timeStamp;
        private int m_randomNumber;

        private const byte TIME_STAMP_ITERATOR = 160;
        private const byte PACKAGE_SIZE = 172;
        private const byte WAVE_INDEX = 12;
        private const byte HIGH_BYTE_SHIFT = 24;
        private const byte MIDDLE_BYTE_SHIFT = 16;
        private const byte SMALL_BYTE_SHIFT = 8;

        public PackageCreator(byte[] header)
            : this()
        {
            if (header == null) throw new ArgumentNullException("header");
            m_header = header;
        }

        private PackageCreator()
        {
            m_packageNumber = 0;
            m_timeStamp = 0;
            ChangeRandom();
        }

        public byte[] GetPackage(byte[] waveBytes)
        {
            m_packageNumber++;
            m_timeStamp += TIME_STAMP_ITERATOR;

            var package = new byte[PACKAGE_SIZE];
            package[0] = m_header[0];
            package[1] = m_header[1];
            package[2] = (byte)(m_packageNumber >> SMALL_BYTE_SHIFT);
            package[3] = (byte)m_packageNumber;
            package[4] = (byte)(m_timeStamp >> HIGH_BYTE_SHIFT);
            package[5] = (byte)(m_timeStamp >> MIDDLE_BYTE_SHIFT);
            package[6] = (byte)(m_timeStamp >> SMALL_BYTE_SHIFT);
            package[7] = (byte)m_timeStamp;
            package[8] = (byte)(m_randomNumber >> HIGH_BYTE_SHIFT);
            package[9] = (byte)(m_randomNumber >> MIDDLE_BYTE_SHIFT);
            package[10] = (byte)(m_randomNumber >> SMALL_BYTE_SHIFT);
            package[11] = (byte)m_randomNumber;

            for (byte arrayIterator = WAVE_INDEX, waveIterator = 0; arrayIterator < PACKAGE_SIZE; waveIterator++)
            {
                package[arrayIterator++] = waveBytes[waveIterator];
            }

            return package;
        }

        public void ChangeRandom()
        {
            m_randomNumber = new Random().Next();
        }
    }
}