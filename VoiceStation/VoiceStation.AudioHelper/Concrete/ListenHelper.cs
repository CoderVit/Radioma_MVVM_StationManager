﻿using System;
using System.Linq;
using VoiceStation.AudioHelper.Abstractions;
using VoiceStation.AudioHelper.Models;
using VoiceStation.Codec.Abstraction;
using VoiceStation.Convertors.Abstract;
using VoiceStation.Package.Abstractions;

namespace VoiceStation.AudioHelper.Concrete
{
    public class ListenHelper : IListenHelper
    {
        public event Action<ListenStatus> OnUpdateListenStatus;
        public event Action<ushort> OnOutValueUpdated;

        private readonly IG711 m_codec;
        private readonly IPackageHandler m_packageHandler;
        private readonly IPlayer m_player;
        private readonly ILogarithmConvertor m_logarithmConvertor;

        public WaveOutMode WaveOutMode { get; set; }

        private readonly System.Timers.Timer m_timer;

        public ListenHelper(IG711 codec, IPackageHandler packageHandler, IPlayer player, ILogarithmConvertor logarithmConvertor)
            : this()
        {
            if (codec == null) throw new ArgumentNullException("codec");
            m_codec = codec;
            if (packageHandler == null) throw new ArgumentNullException("packageHandler");
            m_packageHandler = packageHandler;
            if (player == null) throw new ArgumentNullException("player");
            m_player = player;
            if (logarithmConvertor == null) throw new ArgumentNullException("logarithmConvertor");
            m_logarithmConvertor = logarithmConvertor;
        }

        private ListenHelper()
        {
            m_timer = new System.Timers.Timer
            {
                AutoReset = true
            };
            m_timer.Elapsed += (sender, args) =>
            {
                OnOutValueUpdated(0);
                OnUpdateListenStatus(ListenStatus.LISTEN_STATUS_ERROR);
            };
        }

        public void ListenStart()
        {
            m_timer.Start();
        }

        public void HandleAudio(byte[] data)
        {
            m_packageHandler.CheckSequence(data[2], data[3]);

            m_timer.Interval = 200;

            if (m_packageHandler.IsNative(data[10], data[11]))
            {
                OnUpdateListenStatus(ListenStatus.LISTEN_STATUS_RADIO);
                if (WaveOutMode == WaveOutMode.WAVE_OUT_MODE_OFF) return;
            }
            else
            {
                OnUpdateListenStatus(ListenStatus.LISTEN_STATUS_INTERCOM);
                if (WaveOutMode != WaveOutMode.WAVE_OUT_MODE_PLAY_INTERCOM) return;
            }
            var alawData = m_codec.UlawToLinear(data.Skip(12), ChannelVolumeLevel);
            OnOutValueUpdated(alawData.Dispersion);
            m_player.AddSamples(alawData.Waves);
        }

        public void Play()
        {
            m_player.Play();
        }

        public void Stop()
        {
            OnOutValueUpdated(0);
            m_player.Stop();
        }

        #region Properties

        private double m_channelVolumeLevel;
        public double ChannelVolumeLevel
        {
            get
            {
                return m_channelVolumeLevel;
            }
            set
            {
                m_channelVolumeLevel = m_logarithmConvertor.Convert(value);
            }
        }

        #endregion Properties
    }
}