﻿using VoiceStation.Config.Models;

namespace VoiceStation.Config.Abstract
{
    public interface IConfigHelper
    {
        StationConfig Read();
    }
}