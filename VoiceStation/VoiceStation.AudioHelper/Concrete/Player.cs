﻿using NAudio.Wave;
using System;
using VoiceStation.AudioHelper.Abstractions;

namespace VoiceStation.AudioHelper.Concrete
{
    public class Player : IPlayer
    {
        private readonly IWavePlayer m_wavePlayer;
        private readonly BufferedWaveProvider m_waveProvider;

        public Player(IWavePlayer wavePlayer, BufferedWaveProvider waveProvider)
        {
            if (wavePlayer == null) throw new ArgumentNullException("wavePlayer");
            m_wavePlayer = wavePlayer;
            if (waveProvider == null) throw new ArgumentNullException("waveProvider");
            m_waveProvider = waveProvider;
        }

        public void Play()
        {
            m_wavePlayer.Play();
        }

        public void Stop()
        {
            m_wavePlayer.Stop();
            m_waveProvider.ClearBuffer();
        }

        public void AddSamples(byte[] samples)
        {
            m_waveProvider.AddSamples(samples, 0, samples.Length);
        }
    }
}