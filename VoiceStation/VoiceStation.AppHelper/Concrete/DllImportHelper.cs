﻿using System;
using System.Runtime.InteropServices;
using VoiceStation.AppHelper.Abstract;

namespace VoiceStation.AppHelper.Concrete
{
    /// <summary>
    /// Класс обертка для вызовы windows dlls
    /// </summary>
    public class DllImportHelper : IDllImportHelper
    {
        [DllImport("user32.dll", EntryPoint = "SetForegroundWindow")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Установка окна поверх всех открытых окон
        /// </summary>
        /// <param name="windowHeader"></param>
        public void ShowWindow(IntPtr windowHeader)
        {
            SetForegroundWindow(windowHeader);
        }
    }
}
