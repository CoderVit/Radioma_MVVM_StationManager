﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.NetworkClients.Concrete
{
    public class CommonClient : ICommonClient
    {
        public event Action<bool> OnStatusUpdate;
        public event Action<byte[]> OnAudioReceived;
        public event Action<bool> OnPttStatusUpdate;

        private readonly IClient m_client;

        private CancellationTokenSource m_pingCancellationToken;

        private readonly byte[] m_pingText = new byte[0];
        private readonly System.Timers.Timer m_timer;

        private const ushort PING_PERIOD = 1000;
        private const byte MAX_ERROR_COUNT = 3;
        private const string SUCCESS_RESPONSE = "OK";

        private byte m_errorCount;

        public CommonClient(IClient client)
            : this()
        {
            if (client == null) throw new ArgumentNullException("client");
            m_client = client;
        }

        private CommonClient()
        {
            m_timer = new System.Timers.Timer
            {
                Interval = PING_PERIOD + 200
            };
            m_timer.Elapsed += (sender, args) =>
            {
                if (++m_errorCount != MAX_ERROR_COUNT)
                {
                    Debug.WriteLine("~~~~~~~~~~~~~~~one time timer elapsed {0}", m_errorCount);
                    return;
                }
                Debug.WriteLine("---------------error elapsed {0}", m_errorCount);
                OnStatusUpdate(false);
                m_errorCount = 0;
            };
            m_timer.Start();
        }

        public void PingStart()
        {
            m_pingCancellationToken = new CancellationTokenSource();
            Task.Factory.StartNew(() =>
            {
                while (!m_pingCancellationToken.IsCancellationRequested)
                {
                    m_client.Send(m_pingText);
                    Thread.Sleep(PING_PERIOD);
                }
            }, m_pingCancellationToken.Token);
        }

        public void PingStop()
        {
            m_pingCancellationToken.Cancel();
        }

        public void RxStart()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    try
                    {
                        var data = m_client.Receive();
                        m_timer.Stop();
                        m_errorCount = 0;
                        switch (data.Length)
                        {
                            case 0:
                                OnStatusUpdate(true);
                                break;
                            case 172:
                                OnAudioReceived(data);
                                break;
                            case 2:
                            case 4:
                                OnPttStatusUpdate(Encoding.ASCII.GetString(data) == SUCCESS_RESPONSE);
                                break;
                            default:
                                throw new NotSupportedException("Unsupported received data Exception");
                        }
                        m_timer.Start();
                    }
                    catch { }
                }
            }, TaskCreationOptions.LongRunning);
        }
    }
}