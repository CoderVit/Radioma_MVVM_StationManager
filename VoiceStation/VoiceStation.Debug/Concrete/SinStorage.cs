﻿using System;
using VoiceStation.Engineer.SinHelper.Abstract;

namespace VoiceStation.Engineer.SinHelper.Concrete
{
    public class SinStorage : ISinStorage
    {
        private readonly short[] m_sinWaveBuffer;
        private short m_current;

        public SinStorage(short[] waveBuffer)
        {
            if (waveBuffer == null) throw new ArgumentNullException("waveBuffer");
            m_sinWaveBuffer = waveBuffer;
        }

        public short GetPoint()
        {
            var value = m_sinWaveBuffer[m_current++];
            if (m_current == 8) m_current = 0;
            return value;
        }
    }
}