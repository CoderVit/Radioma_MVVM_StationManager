﻿using System;

namespace VoiceStation.AppHelper.Abstract
{
    public interface IDllImportHelper
    {
        void ShowWindow(IntPtr windowHeader);
    }
}
