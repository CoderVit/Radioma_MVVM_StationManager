﻿namespace VoiceStation.AudioHelper.Models
{
    public enum ListenStatus
    {
        LISTEN_STATUS_ERROR,
        LISTEN_STATUS_RADIO,
        LISTEN_STATUS_INTERCOM
    }
}