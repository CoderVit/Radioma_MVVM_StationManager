﻿using System;
using System.Threading;
using System.Threading.Tasks;
using VoiceStation.Common.Abstract;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models
{
    public class TimeViewModel : ViewModel
    {
        private readonly ITime m_time;

        public TimeViewModel(ITime time)
        {
            if (time == null) throw new ArgumentNullException("time");
            m_time = time;
        }

        public TimeViewModel ModelRun()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    CurrentTime = m_time.Now;
                    Thread.Sleep((60 - m_time.Now.Second) * 1000 - m_time.Now.Millisecond);
                }
            });
            return this;
        }

        #region Properties

        private DateTime m_currentTime;
        public DateTime CurrentTime
        {
            get { return m_currentTime; }
            set
            {
                m_currentTime = value;
                RaisePropertyChanged("CurrentTime");
            }
        }

        #endregion Properties
    }
}