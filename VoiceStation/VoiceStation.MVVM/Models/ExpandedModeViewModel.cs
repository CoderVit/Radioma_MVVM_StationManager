﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using VoiceStation.ExpandedMode.Abstract;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models
{
    public class ExpandedModeViewModel : ViewModel
    {
        private readonly IRrcHelper m_rrcHelper;

        public ExpandedModeViewModel(IRrcHelper rrcHelper)
        {
            if (rrcHelper == null) throw new ArgumentNullException("rrcHelper");
            m_rrcHelper = rrcHelper;
        }

        private void EnableExpandMode()
        {
            ExpandedModeStatement = ExpandedModeStatement.EXPANDED_MODE_STATEMENT_REQUEST;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    m_rrcHelper.TryToEnable();
                    m_rrcHelper.RealTimeWorkStart();
                    ExpandedModeStatement = ExpandedModeStatement.EXPANDED_MODE_STATEMENT_ON;
                }
                catch (Exception)
                {
                    ExpandedModeStatement = ExpandedModeStatement.EXPANDED_MODE_STATEMENT_ERROR;
                }
            });
        }

        #region Commands

        public ICommand EnableModeCommand { get { return new StationCommand(EnableExpandMode); } }

        #endregion Commands

        #region Properties

        private ExpandedModeStatement m_expandedModeStatement;
        public ExpandedModeStatement ExpandedModeStatement
        {
            get { return m_expandedModeStatement; }
            set
            {
                m_expandedModeStatement = value;
                RaisePropertyChanged("ExpandedModeStatement");
            }
        }

        #endregion Properties
    }
}