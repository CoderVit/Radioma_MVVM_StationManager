﻿using System.Collections.Generic;
using VoiceStation.AppCreator.Builders;
using VoiceStation.Multiton.Abstract;
using VoiceStation.Multiton.Concrete;
using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.AppCreator.Creators
{
    public class WaveSenderPoolBuilder
    {
        private WaveSenderBuilder m_waveSenderBuilder;

        #region Builder Methods

        public WaveSenderPoolBuilder WithWaveSenderBuilder(WaveSenderBuilder waveSenderBuilder)
        {
            m_waveSenderBuilder = waveSenderBuilder;
            return this;
        }

        #endregion Builder Methods

        public IWaveSenderPool Build()
        {
            return new WaveSenderPool(new Dictionary<ChannelNumber, IWaveSender>
            {
                { ChannelNumber.CHANNEL_NUMBER_FIRST, m_waveSenderBuilder.Build(ChannelNumber.CHANNEL_NUMBER_FIRST) },
                { ChannelNumber.CHANNEL_NUMBER_SECOND, m_waveSenderBuilder.Build(ChannelNumber.CHANNEL_NUMBER_SECOND) },
                { ChannelNumber.CHANNEL_NUMBER_THIRD, m_waveSenderBuilder.Build(ChannelNumber.CHANNEL_NUMBER_THIRD) },
                { ChannelNumber.CHANNEL_NUMBER_FOURTH, m_waveSenderBuilder.Build(ChannelNumber.CHANNEL_NUMBER_FOURTH) }
            });
        }
    }
}