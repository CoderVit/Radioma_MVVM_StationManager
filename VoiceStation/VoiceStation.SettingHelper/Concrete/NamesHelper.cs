﻿using System;
using System.Collections.Generic;
using System.IO;
using VoiceStation.SettingHelper.Abstract;

namespace VoiceStation.SettingHelper.Concrete
{
    public class NamesHelper : INamesHelper
    {
        private readonly IEncryptManager m_encryptManager;
        private readonly string m_defaultPath;

        private readonly string[] m_defaultNames;

        public NamesHelper(IEncryptManager encryptManager, string defaultPath)
            : this()
        {
            if (encryptManager == null) throw new ArgumentNullException("encryptManager");
            m_encryptManager = encryptManager;
            if (defaultPath == null) throw new ArgumentNullException("defaultPath");
            m_defaultPath = defaultPath;
        }

        private NamesHelper()
        {
            m_defaultNames = new[] { "IC-F8101", "IC-A120", "IC-F5061D", "IC-F6061D" };
        }

        public IEnumerable<string> GetNames()
        {
            try
            {
                using (var reader = new StreamReader(m_defaultPath))
                {
                    return m_encryptManager.Decrypt(reader.ReadToEnd()).Split('|');
                }
            }
            catch
            {
                return m_defaultNames;
            }
        }

        public void SetNames(IEnumerable<string> names)
        {
            File.WriteAllText(m_defaultPath, m_encryptManager.Encrypt(string.Join("|", names)));
        }
    }
}