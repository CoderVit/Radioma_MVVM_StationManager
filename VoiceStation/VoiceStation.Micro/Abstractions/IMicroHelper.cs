﻿using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.Micro.Abstractions
{
    public interface IMicroHelper
    {
        IWaveSender this[byte index] { set; }
        //void AddSender(IWaveSender sender);
        void PttStart(ChannelNumber channel);
        void PttStop(ChannelNumber channel);
    }
}