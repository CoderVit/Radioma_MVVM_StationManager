﻿using VoiceStation.AppCreator.Creators;
using VoiceStation.Convertors.Abstract;
using VoiceStation.Convertors.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class DispersionHelperBuilder
    {
        private DispersionBufferCreator m_dispersionBufferBuilder;

        #region Builder Methods

        public DispersionHelperBuilder WithDispersionBufferBuilder(DispersionBufferCreator dispersionBufferBuilder)
        {
            m_dispersionBufferBuilder = dispersionBufferBuilder;
            return this;
        }

        #endregion Builder Methods

        public IDispersionHelper Build()
        {
            return new DispersionHelper(m_dispersionBufferBuilder.Create());
        }
    }
}
