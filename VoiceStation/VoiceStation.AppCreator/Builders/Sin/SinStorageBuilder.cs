﻿using VoiceStation.Engineer.SinHelper.Abstract;
using VoiceStation.Engineer.SinHelper.Concrete;

namespace VoiceStation.AppCreator.Builders.Sin
{
    public class SinStorageBuilder
    {
        public ISinStorage Create()
        {
            return new SinStorage(new short[] { 0, 23161, 32766, 23197, 52, -23124, -32766, -23234 });
        }
    }
}