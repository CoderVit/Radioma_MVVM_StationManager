﻿using VoiceStation.AppHelper.Abstract;
using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Builders
{
    public class InvokeRrcBuilder
    {
        private IPathHelper m_pathHelper;
        private IProcessInvoker m_processInvoker;

        #region Builder Methods

        public InvokeRrcBuilder WithProcessInvoker(IProcessInvoker invoker)
        {
            m_processInvoker = invoker;
            return this;
        }

        public InvokeRrcBuilder WithPathHelper(IPathHelper pathHelper)
        {
            m_pathHelper = pathHelper;
            return this;
        }

        #endregion Builder Methods

        public InvokeRrcViewModel Create()
        {
            var vm = new InvokeRrcViewModel(m_processInvoker)
            {
                IsInvokePossible = m_pathHelper.IsFileExist
            };
            return vm;
        }
    }
}