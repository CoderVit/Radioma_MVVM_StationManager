﻿using VoiceStation.AppCreator.Creators;
using VoiceStation.MVVM.Models;

namespace VoiceStation.AppCreator.Builders.ViewModels
{
    public class VolumeVmBuilder
    {
        private VolumeConvertorCreator m_volumeConvertorBuilder;
        private SystemVolumeManagerBuilder m_volumeManagerBuilder;

        #region Builder Methods

        public VolumeVmBuilder WithVolumeConvertorBuilder(VolumeConvertorCreator volumeConvertorBuilder)
        {
            m_volumeConvertorBuilder = volumeConvertorBuilder;
            return this;
        }

        public VolumeVmBuilder WithVolumeManagerBuilder(SystemVolumeManagerBuilder volumeManagerBuilder)
        {
            m_volumeManagerBuilder = volumeManagerBuilder;
            return this;
        }

        #endregion Builder Methods

        public VolumeViewModel Build()
        {
            //TODO remove belowing instance
            var manager = m_volumeManagerBuilder.Build();
            return new VolumeViewModel(manager, m_volumeConvertorBuilder.Create())
            {
                IsMute = manager.IsMute,
                VolumeLevel = manager.SystemVolume
            }.ViewModelStart();
        }
    }
}
