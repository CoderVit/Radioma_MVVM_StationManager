﻿namespace VoiceStation.Handler.Models
{
    public enum PttStatement
    {
        PTT_STATEMENT_COMMON = 18,
        PTT_STATEMENT_FIRST = 35,
        PTT_STATEMENT_SECOND = 36,
        PTT_STATEMENT_THIRD = 37,
        PTT_STATEMENT_FOURTH = 38
    }
}