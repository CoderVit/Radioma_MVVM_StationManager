﻿using System.ComponentModel;

namespace VoiceStation.MVVM.Models.Base
{
    /// <summary>
    /// базовый класс для моделей представления.
    /// реализующий интерфейс INotifyPropertyChanged
    /// </summary>
    public class ViewModel : INotifyPropertyChanged
    {
        #region MVVM related

        protected void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null && !string.IsNullOrEmpty(propertyName))
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion MVVM related
    }
}
