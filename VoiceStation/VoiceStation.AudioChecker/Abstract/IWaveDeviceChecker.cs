﻿namespace VoiceStation.AudioChecker.Abstract
{
    public interface IWaveDeviceChecker
    {
        byte WaveInNumber { get; }
        byte WaveOutNumber { get; }
    }
}