﻿using System;
using System.Text;
using VoiceStation.SettingHelper.Abstract;
using VoiceStation.SettingHelper.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class NamesHelperBuilder
    {
        private EncryptManagerBuilder m_encryptManagerBuilder;
        private string m_settingFileLocation;

        #region Builder Methods

        public NamesHelperBuilder WithEncryptManager(EncryptManagerBuilder encryptManagerBuilder)
        {
            m_encryptManagerBuilder = encryptManagerBuilder;
            return this;
        }

        public NamesHelperBuilder WithSettingPath(string fileName)
        {
            m_settingFileLocation = new StringBuilder(AppDomain.CurrentDomain.BaseDirectory).Append(fileName).Append(".dat").ToString();
            return this;
        }

        #endregion Builder Methods

        public INamesHelper Build()
        {
            return new NamesHelper(
                m_encryptManagerBuilder.Build(),
                m_settingFileLocation);
        }
    }
}