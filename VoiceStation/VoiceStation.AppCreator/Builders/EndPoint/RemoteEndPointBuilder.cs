﻿using System.Net;
using VoiceStation.Multiton.Models;

namespace VoiceStation.AppCreator.Builders.EndPoint
{
    public class RemoteEndPointBuilder
    {
        private IPAddress[] m_ipAddresses;
        private ushort[] m_ports;

        #region Builder Methods

        public RemoteEndPointBuilder WithAddresses(IPAddress[] ipAddresses)
        {
            m_ipAddresses = ipAddresses;
            return this;
        }

        public RemoteEndPointBuilder AndPorts(ushort[] ports)
        {
            m_ports = ports;
            return this;
        }

        #endregion Builder Methods

        public IPEndPoint Build(ChannelNumber channel)
        {
            return new IPEndPoint(m_ipAddresses[(byte)channel], m_ports[(byte)channel]);
        }
    }
}