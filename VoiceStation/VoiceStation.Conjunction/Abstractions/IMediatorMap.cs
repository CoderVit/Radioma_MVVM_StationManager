﻿using System;
using System.Collections.Generic;
using VoiceStation.Conjunction.Models;

namespace VoiceStation.Conjunction.Abstractions
{
    public interface IMediatorMap
    {
        void AddAction(StationMessage message, Action action);
        List<Action> GetActions(StationMessage message);
    }
}