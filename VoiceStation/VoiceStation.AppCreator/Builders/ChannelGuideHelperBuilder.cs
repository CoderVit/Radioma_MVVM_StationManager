﻿using VoiceStation.ExpandedMode;
using VoiceStation.ExpandedMode.Abstract;
using VoiceStation.Multiton.Models;

namespace VoiceStation.AppCreator.Builders
{
    public class ChannelGuideHelperBuilder
    {
        private string[] m_connectionStrings;

        #region Builder Methods

        public ChannelGuideHelperBuilder WithConnectionStrings(string[] strings)
        {
            m_connectionStrings = strings;
            return this;
        }

        #endregion Builder Methods

        public IChannelGuideHelper Build(ChannelNumber number)
        {
            return new ChannelGuideHelper(m_connectionStrings[(int)number]);
        }
    }
}