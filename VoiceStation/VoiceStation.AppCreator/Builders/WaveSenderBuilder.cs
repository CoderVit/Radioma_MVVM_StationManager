﻿using VoiceStation.AppCreator.Builders.UdpClients;
using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;
using VoiceStation.NetworkClients.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class WaveSenderBuilder
    {
        private ControlClientBuilder m_controlClientBuilder;
        private PackageCreatorBuilder m_packageCreatorBuilder;

        #region Builder Methods

        public WaveSenderBuilder WithClientBuilder(ControlClientBuilder controlClientBuilder)
        {
            m_controlClientBuilder = controlClientBuilder;
            return this;
        }

        public WaveSenderBuilder WithPackageCreatorBuilder(PackageCreatorBuilder packageCreatorBuilder)
        {
            m_packageCreatorBuilder = packageCreatorBuilder;
            return this;
        }

        #endregion Builder Methods

        public IWaveSender Build(ChannelNumber channel)
        {
            return new WaveSender(m_controlClientBuilder.Build(channel), m_packageCreatorBuilder.Build());
        }
    }
}