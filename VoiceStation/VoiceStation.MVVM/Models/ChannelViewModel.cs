﻿using System;
using System.Windows.Input;
using VoiceStation.AudioHelper.Abstractions;
using VoiceStation.AudioHelper.Models;
using VoiceStation.Micro.Abstractions;
using VoiceStation.Multiton.Models;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.MVVM.Models
{
    public class ChannelViewModel : ViewModel
    {
        private readonly ICommonClient m_commonClient;
        private readonly IListenHelper m_listenHelper;
        private readonly ChannelNumber m_channel;
        private readonly IMicroHelper m_microHelper;

        public event Action OnMousePressed;

        public ChannelViewModel(ICommonClient commonClient, IListenHelper listenHelper, IMicroHelper microHelper, ChannelNumber channel)
        {
            if (commonClient == null) throw new ArgumentNullException("commonClient");
            m_commonClient = commonClient;
            if (listenHelper == null) throw new ArgumentNullException("listenHelper");
            m_listenHelper = listenHelper;
            if (microHelper == null) throw new ArgumentNullException("microHelper");
            m_microHelper = microHelper;
            m_channel = channel;
            VolumeLevel = .5;
        }

        public void PttStart()
        {
            // Debug.WriteLine("VM PttStart");
            if (!IsStationConnected || !IsMainPressed || IsPttPressed || ConnectionStatus == ListenStatus.LISTEN_STATUS_INTERCOM) return;
            //  Debug.WriteLine("VM PttStart after validation");
            m_commonClient.PingStop();
            ListenStop();
            m_microHelper.PttStart(m_channel);
            IsPttPressed = true;
        }

        public void PttStop()
        {
            if (!IsPttPressed) return;
            m_commonClient.PingStart();
            ListenStart();
            m_microHelper.PttStop(m_channel);
            IsPttPressed = false;
        }

        #region Private Methods

        private void ListenStart()
        {
            m_listenHelper.WaveOutMode = CurrentMode;
            m_listenHelper.Play();
        }

        private void ListenStop()
        {
            m_listenHelper.WaveOutMode = WaveOutMode.WAVE_OUT_MODE_OFF;
            m_listenHelper.Stop();
        }

        private void ListenToggleStop()
        {
            ListenStop();
            PttStop();
        }

        private WaveOutMode CurrentMode { get { return IsIntercomPressed ? WaveOutMode.WAVE_OUT_MODE_PLAY_INTERCOM : WaveOutMode.WAVE_OUT_MODE_PLAY; } }

        #endregion Private Methods

        #region Commands

        public ICommand ListenToggleCommand { get { return new StationCommand(() => (IsMainPressed ? (Action)ListenStart : ListenToggleStop)()); } }

        public ICommand IntercomToggleCommand { get { return new StationCommand(() => m_listenHelper.WaveOutMode = CurrentMode); } }

        public ICommand PttToggleCommand { get { return new StationCommand(OnMousePressed); } }

        public ICommand IncreaseVolumeCommand { get { return new StationCommand(() => VolumeLevel = VolumeLevel + (float)0.01 < 1 ? VolumeLevel + (float)0.01 : 1); } }

        public ICommand DecreaseVolumeCommand { get { return new StationCommand(() => VolumeLevel = VolumeLevel - (float)0.01 > 0 ? VolumeLevel - (float)0.01 : 0); } }

        #endregion Commands

        #region Properties

        private bool m_isStationConnected;
        public bool IsStationConnected
        {
            get
            {
                return m_isStationConnected;
            }
            set
            {
                if (m_isStationConnected == value) return;
                m_isStationConnected = value;
                RaisePropertyChanged("IsStationConnected");
            }
        }

        private ListenStatus m_connectionStatus;
        public ListenStatus ConnectionStatus
        {
            get
            {
                return m_connectionStatus;
            }
            set
            {
                if (m_connectionStatus == value) return;
                m_connectionStatus = value;
                RaisePropertyChanged("ConnectionStatus");
            }
        }

        private decimal m_frequency;
        public decimal Frequency
        {
            get { return m_frequency; }
            set
            {
                m_frequency = value;
                RaisePropertyChanged("Frequency");
            }
        }

        //TODO try to remove and IsEnabled bind without this variable
        private bool m_isMainPressed;
        public bool IsMainPressed
        {
            get { return m_isMainPressed; }
            set
            {
                m_isMainPressed = value;
                RaisePropertyChanged("IsMainPressed");
            }
        }

        //TODO validate user this properrty or no
        private bool m_isIntercomPressed;
        public bool IsIntercomPressed
        {
            get
            {
                return m_isIntercomPressed;
            }
            set
            {
                m_isIntercomPressed = value;
                RaisePropertyChanged("IsIntercomPressed");
            }
        }

        private bool m_isPttPressed;
        public bool IsPttPressed
        {
            get
            {
                return m_isPttPressed;
            }
            set
            {
                m_isPttPressed = value;
                RaisePropertyChanged("IsPttPressed");
            }
        }

        private double m_waveOutVolume;
        public double VolumeLevel
        {
            get { return m_waveOutVolume; }
            set
            {
                m_waveOutVolume = value;
                m_listenHelper.ChannelVolumeLevel = value;
                RaisePropertyChanged("VolumeLevel");
            }
        }

        private ushort m_outLevel;
        public ushort OutLevel
        {
            get { return m_outLevel; }
            set
            {
                if (m_outLevel == value) return;
                m_outLevel = value;
                RaisePropertyChanged("OutLevel");
            }
        }

        #endregion Properties
    }
}
