﻿using System;
using System.Windows.Input;
using VoiceStation.MVVM.Commands;
using VoiceStation.MVVM.Models.Base;

namespace VoiceStation.MVVM.Models
{
    public class SettingsViewModel : ViewModel
    {
        public Action onDebugButtonClick;
        public Action onUserSettingButtonClick;

        #region Commands

        public ICommand SettingWindowCommand { get { return new StationCommand(onUserSettingButtonClick); } }

        public ICommand DebugWindowCommand { get { return new StationCommand(onDebugButtonClick); } }

        #endregion Commands

        #region Properties

        private bool m_isEngineerMode;
        public bool IsEngineerMode
        {
            get { return m_isEngineerMode; }
            set
            {
                m_isEngineerMode = value;
                RaisePropertyChanged("IsEngineerMode");
            }
        }

        #endregion Properties
    }
}
