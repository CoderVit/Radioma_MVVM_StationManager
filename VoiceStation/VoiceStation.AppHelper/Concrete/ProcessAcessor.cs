﻿using System.Diagnostics;
using System.Linq;
using VoiceStation.AppHelper.Abstract;

namespace VoiceStation.AppHelper.Concrete
{
    public class ProcessAcessor : IProcessAcessor
    {
        public Process CurrentProcess { get { return Process.GetCurrentProcess(); } }

        public Process[] Processes { get { return Process.GetProcesses(); } }

        public Process GetProcessByName(string name)
        {
            return Process.GetProcessesByName(name).FirstOrDefault();
        }
    }
}