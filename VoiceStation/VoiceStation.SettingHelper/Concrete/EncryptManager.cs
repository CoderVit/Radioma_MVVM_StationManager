﻿using System;
using System.Security.Cryptography;
using System.Text;
using VoiceStation.SettingHelper.Abstract;

namespace VoiceStation.SettingHelper.Concrete
{
    public class EncryptManager : IEncryptManager
    {
        private readonly byte[] m_salt;

        public EncryptManager(byte[] salt)
        {
            if (salt == null) throw new ArgumentNullException("salt");
            m_salt = salt;
        }

        public string Encrypt(string data)
        {
            var cipherBytes = ProtectedData.Protect(Encoding.UTF8.GetBytes(data), m_salt, DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(cipherBytes);
        }

        public string Decrypt(string data)
        {
            var passwordBytes = ProtectedData.Unprotect(Convert.FromBase64String(data), m_salt, DataProtectionScope.CurrentUser);
            return Encoding.UTF8.GetString(passwordBytes);
        }
    }
}