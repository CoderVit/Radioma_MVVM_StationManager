﻿using System.Collections.Generic;

namespace VoiceStation.SettingHelper.Abstract
{
    public interface INamesHelper
    {
        IEnumerable<string> GetNames();
        void SetNames(IEnumerable<string> names);
    }
}