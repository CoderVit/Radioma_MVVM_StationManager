﻿namespace VoiceStation.Convertors.Abstract
{
    public interface IVolumeConvertor
    {
        byte FromSystemVolume(float value);
        float ToSystemVolume(byte value);
    }
}