﻿using System.Collections.Generic;
using VoiceStation.Codec.Abstraction;
using VoiceStation.Codec.Models;

namespace VoiceStation.Codec.Concrete
{
    public class G711 : IG711
    {
        private const byte SIGN_BIT = 0x80;
        private const ushort QUANT_MASK = 0xF;
        private const ushort SEG_SHIFT = 4;
        private const ushort SEG_MASK = 0x70;
        private const int BIAS = 0x84;
        private const byte TABLE_SIZE = 8;

        private readonly byte m_packageLength;
        private readonly ushort m_alawPackageLength;


        private readonly ushort m_dispersionMax;
        private readonly short[] m_searhTable;

        public G711()
        {
            m_packageLength = 160;
            m_alawPackageLength = (ushort)(m_packageLength * 2);
            m_dispersionMax = 15000;
            m_searhTable = new short[] { 0x3F, 0x7F, 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF, 0x1FFF };
        }

        //unpack voice before playing
        public Linear UlawToLinear(IEnumerable<byte> waveBuffer, double volumeLevel)
        {
            var bytesLinear = new byte[m_alawPackageLength];

            var elementsSum = 0;
            var elementsMultiplicable = 0;
            var bytesLinearIterator = 0;

            foreach (var wave in waveBuffer)
            {
                /* Complement to obtain normal u-law value. */
                var uVal = ~wave;

                /*
                 * Extract and bias the quantization bits. Then
                 * shift up by the segment number and subtract out the bias.
                 */
                var t = ((uVal & QUANT_MASK) << 3) + BIAS;
                t <<= (uVal & SEG_MASK) >> SEG_SHIFT;

                var element = (short)(((uVal & SIGN_BIT) >= 128 ? BIAS - t : t - BIAS) * volumeLevel);
                bytesLinear[bytesLinearIterator++] = (byte)element;
                bytesLinear[bytesLinearIterator++] = (byte)(element >> 8);

                var str = element >> 6;
                elementsSum += str;
                elementsMultiplicable += str * str;
            }

            var m = elementsSum / m_packageLength;
            var dispersion = (uint)(elementsMultiplicable / m_packageLength - m * m);

            return new Linear
            {
                Waves = bytesLinear,
                Dispersion = dispersion <= m_dispersionMax ? (ushort)dispersion : m_dispersionMax
            };
        }

        //pack voice before sending
        public byte[] LinearToUlaw(short[] pcmValues)
        {
            var ullows = new byte[pcmValues.Length];

            for (var pcmIterator = 0; pcmIterator < pcmValues.Length; pcmIterator++)
            {
                var pcmValue = pcmValues[pcmIterator];
                short mask;

                pcmValue = (short)(pcmValue >> 2);

                if (pcmValue < 0)
                {
                    pcmValue *= -1;
                    mask = 0x7F;
                }
                else
                {
                    mask = 0xFF;
                }

                if (pcmValue > 8159) pcmValue = 8159;		/* clip the magnitude */
                pcmValue += 0x84 >> 2;

                /* Convert the scaled magnitude to segment number. */
                var segment = Search(pcmValue);

                /*
                 * Combine the sign, segment, quantization bits;
                 * and complement the code word.
                 */
                if (segment >= 8)		/* out of range, return maximum value. */
                    ullows[pcmIterator] = (byte)(0x7F ^ mask);
                else
                {
                    ullows[pcmIterator] = (byte)(((segment << 4) | ((pcmValue >> (segment + 1)) & 0xF)) ^ mask);
                }
            }

            return ullows;
        }

        private short Search(short val)
        {
            for (short i = 0; i < TABLE_SIZE; i++)
            {
                if (val <= m_searhTable[i])
                {
                    return i;
                }
            }
            return TABLE_SIZE;
        }
    }
}