﻿using System;
using System.Collections.Generic;
using System.Linq;
using VoiceStation.Multiton.Abstract;
using VoiceStation.Multiton.Models;
using VoiceStation.NetworkClients.Abstractions;

namespace VoiceStation.Multiton.Concrete
{
    public class WaveSenderPool : IWaveSenderPool
    {
        private readonly Dictionary<ChannelNumber, IWaveSender> m_channelSenders;

        public bool AnyActive { get { return m_channelSenders.Any(c => c.Value.IsCanSend); } }

        public WaveSenderPool(Dictionary<ChannelNumber, IWaveSender> channelSenders)
        {
            if (channelSenders == null) throw new ArgumentNullException("channelSenders");
            m_channelSenders = channelSenders;
        }

        public void Add(ChannelNumber channel)
        {
            m_channelSenders[channel].IsCanSend = true;
        }

        public void Release(ChannelNumber channel)
        {
            m_channelSenders[channel].IsCanSend = false;
            m_channelSenders[channel].Stop();
        }

        public void Send(byte[] data)
        {
            foreach (var channelSender in m_channelSenders)
            {
                if (channelSender.Value.IsCanSend) channelSender.Value.Send(data);
            }
        }
    }
}