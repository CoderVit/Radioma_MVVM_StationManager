﻿namespace VoiceStation.MVVM.Models
{
    public enum ExpandedModeStatement
    {
        EXPANDED_MODE_STATEMENT_OFF,
        EXPANDED_MODE_STATEMENT_REQUEST,
        EXPANDED_MODE_STATEMENT_ON,
        EXPANDED_MODE_STATEMENT_ERROR
    }
}