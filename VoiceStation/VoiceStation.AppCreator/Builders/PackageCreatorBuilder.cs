﻿using VoiceStation.Package.Abstractions;
using VoiceStation.Package.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class PackageCreatorBuilder
    {
        private byte[] m_headerBytes;

        #region Builder Methods

        public PackageCreatorBuilder AndHeader(ushort header)
        {
            m_headerBytes = new[] { (byte)header, (byte)(header >> 8) };
            return this;
        }

        #endregion Builder Methods

        public IPackageCreator Build()
        {
            return new PackageCreator(m_headerBytes);
        }
    }
}