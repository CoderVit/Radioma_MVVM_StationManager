﻿namespace VoiceStation.Convertors.Abstract
{
    public interface IDispersionHelper
    {
        ushort Compute(byte[] waves);
    }
}