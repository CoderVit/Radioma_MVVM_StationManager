﻿using System;
using VoiceStation.Convertors.Abstract;

namespace VoiceStation.Convertors.Concrete
{
    public class LogarithmConverter : ILogarithmConvertor
    {
        private readonly double[] m_logValues;

        public LogarithmConverter(double[] logValues)
        {
            if (logValues == null) throw new ArgumentNullException("logValues");
            m_logValues = logValues;
        }

        public double Convert(double value)
        {
            return m_logValues[(byte)(value * 100)];
        }
    }
}