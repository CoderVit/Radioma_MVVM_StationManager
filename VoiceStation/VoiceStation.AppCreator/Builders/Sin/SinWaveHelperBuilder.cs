﻿using VoiceStation.Engineer.SinHelper.Abstract;
using VoiceStation.Engineer.SinHelper.Concrete;

namespace VoiceStation.AppCreator.Builders.Sin
{
    public class SinWaveHelperBuilder
    {
        private SinStorageBuilder m_sinStorageBuilder;

        #region Builder Methods

        public SinWaveHelperBuilder WithSinStorageBuilder(SinStorageBuilder sinStorageBuilder)
        {
            m_sinStorageBuilder = sinStorageBuilder;
            return this;
        }

        #endregion Builder Methods

        public ISinWaveHelper Build()
        {
            return new SinWaveHelper(m_sinStorageBuilder.Create());
        }
    }
}