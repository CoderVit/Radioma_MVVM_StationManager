﻿using VoiceStation.Conjunction.Abstractions;
using VoiceStation.Conjunction.Concrete;

namespace VoiceStation.AppCreator.Builders
{
    public class MediatorBuilder
    {
        private MapperBuilder m_mapperBuilder;

        #region Builder Methods

        public MediatorBuilder WithMediatorMapperBuilder(MapperBuilder mapperBuilder)
        {
            m_mapperBuilder = mapperBuilder;
            return this;
        }

        #endregion Builder Methods

        public IMediator Build()
        {
            return new Mediator(m_mapperBuilder.Build());
        }
    }
}