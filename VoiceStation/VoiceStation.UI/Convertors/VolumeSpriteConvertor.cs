﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace VoiceStation.UI.Convertors
{
    internal class VolumeSpriteConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var volumeLevel = System.Convert.ToDouble(value);

            return new CroppedBitmap(new BitmapImage(new Uri("pack://application:,,,/Content/Images/spritesheet.png", UriKind.Absolute)), new Int32Rect
            {
                Width = 50,
                Height = 50,
                X = volumeLevel <= .33 ? 305 : volumeLevel >= .66 ? 425 : 365,
                Y = 5
            });
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
